package org.mimi.clibrary.Beans.publication;

import java.util.List;

public class BookingsCopiesBean {

    private List<CopyBean> copies;
    private int nbBookings;

    public List<CopyBean> getCopies() {
        return copies;
    }

    public void setCopies(List<CopyBean> copies) {
        this.copies = copies;
    }

    public int getNbBookings() {
        return nbBookings;
    }

    public void setNbBookings(int nbBookings) {
        this.nbBookings = nbBookings;
    }
}
