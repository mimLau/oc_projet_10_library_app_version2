package org.mimi.clibrary.Beans.publication;

public enum BookingStatus {

    INPROGRESS,
    FINISHED,
    CANCELLED;
}
