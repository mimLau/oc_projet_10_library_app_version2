package org.mimi.clibrary.Beans.publication;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookingJsonBean {

    private Integer member;
    private Integer publication;
}
