package org.mimi.clibrary.Beans.publication;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.mimi.clibrary.Beans.account.MemberBean;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookingBean {

    private Integer id;
    private PublicationBean publication;
    private MemberBean member;
    private LocalDate bookingDate;
    private String bookingStatus;
    private PublicationBean publicationBean;
    private int position;

}
