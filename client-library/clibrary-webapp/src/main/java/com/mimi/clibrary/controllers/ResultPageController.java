package com.mimi.clibrary.controllers;

import org.mimi.clibrary.proxies.FeignProxy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;

@Controller
public class ResultPageController {

    private FeignProxy feign;
    private static final String PUBLICATION_VIEW = "publicationResult";

    public ResultPageController(FeignProxy feign) {
        this.feign = feign;
    }

    @GetMapping("/Publication/result")
    public String homePage(HttpSession session, Model model) {
        Object booking = session.getAttribute("booking");
        model.addAttribute("booking", booking);
        session.removeAttribute("booking");
        return PUBLICATION_VIEW;
    }
}
