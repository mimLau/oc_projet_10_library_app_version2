package com.mimi.clibrary.controllers;

import org.mimi.clibrary.proxies.FeignProxy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserHomeController {

    private FeignProxy feignProxy;
    private static final String USER_HOME_VIEW = "userHome";

    public UserHomeController(FeignProxy feignProxy) {
        this.feignProxy = feignProxy;
    }


    @GetMapping("/User/home")
    public String homePage( Model model ) {

        return USER_HOME_VIEW;

    }
}
