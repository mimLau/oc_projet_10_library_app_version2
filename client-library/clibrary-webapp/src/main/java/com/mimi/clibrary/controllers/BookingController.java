package com.mimi.clibrary.controllers;

import feign.FeignException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mimi.clibrary.Beans.account.MemberBean;
import org.mimi.clibrary.Beans.publication.*;
import org.mimi.clibrary.proxies.FeignProxy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class BookingController {

    final static Logger LOGGER  = LogManager.getLogger(BookingController.class);

    private FeignProxy proxy;
    private static final String PUBLICATION_RESULT_PAGE = "redirect:/Publication/result";
    private static final String USER_BOOKINGS_PAGE = "redirect:/UserBookings";
    private static final String USER_BOOKINGS_VIEW = "userBookings";
    private static final String ERROR_BOOKING_R = "Vous avez déjà réservé \n";
    private static final String ERROR_BOOKING_E = "Vous avez déjà emprunté \n";

    public BookingController(FeignProxy proxy) {
        this.proxy = proxy;
    }

    @GetMapping("/Booking")
    public String booking( @RequestParam int idp, @RequestParam int idm, HttpSession session ) {

        BookingJsonBean bookingJsonBean = new BookingJsonBean();
        bookingJsonBean.setMember( idm );
        bookingJsonBean.setPublication( idp );
        List<BookingBean> bookings;

        Map<PublicationBean, BookingsCopiesBean> publicationsBookingsCopies;

        try {
            proxy.addBooking( bookingJsonBean );
            session.setAttribute("booking", true);
            publicationsBookingsCopies = (Map<PublicationBean, BookingsCopiesBean>) session.getAttribute("publicationsBookingsCopies");

            for (Map.Entry<PublicationBean, BookingsCopiesBean> entry : publicationsBookingsCopies.entrySet()) {
                System.out.println("Key = " + entry.getKey() +
                        ", Value = " + entry.getValue());

                if (entry.getKey().getId() == idp) {
                    bookings = proxy.findListBookingByPublicationId( idp , BookingStatus.INPROGRESS );

                    entry.getValue().setNbBookings( bookings.size() );
                }
            }

        } catch ( FeignException f ) {

            LOGGER.error("Impossible to register a booking for pubId: " + idp + " and memberId: " + idm);
            LOGGER.error("Feign exception: " + f.contentUTF8());
            LOGGER.error("Feign exception: " + f.contentUTF8().contains("L'utilisateur a déja réservé cette publication."));
            LOGGER.error("Feign exception: " + f.contentUTF8().contains("L'utilisateur a déja emprunté une copie de la publication."));

            if(f.contentUTF8().contains("L'utilisateur a déja réservé cette publication.") ) {
                session.setAttribute("errorBooking", ERROR_BOOKING_R);
            } else if(f.contentUTF8().contains("L'utilisateur a déja emprunté une copie de la publication.")) {
                session.setAttribute("errorBooking", ERROR_BOOKING_E);
            }

        }

        return PUBLICATION_RESULT_PAGE;
    }

    @GetMapping("/UserBookings")
    public String memberBooking( Model model, HttpSession session ) {

        MemberBean member = (MemberBean) session.getAttribute("user");
        int position;

        try {
            List<BookingBean> bookings = proxy.getListBookingByUserId(member.getId());

            List<CopyBean> copies;
            Map<BookingBean, List<CopyBean>> bookingCopies = new HashMap<>();

        /*
          We want all copies of the booked publication, and retrieve the one who has the earliest returnDate.
          In webservice side, getListCopiesByPubId is ordered in the desc order,
          so the first copy in the list has the earliest return date.
          We can't just do publications.getCopies[0], because in that way, copies aren't ordered by returnDate.
         */

            for (int i = 0; i < bookings.size(); i++) {

                copies = proxy.getAllCopyByPublicationId(bookings.get(i).getPublication().getId());
                LOGGER.info("Copy with earliest returnDate: " + copies.get(0).getReturnDate() + " id: " +  copies.get(0).getId());
                LOGGER.info("Booking n°" + i + " : for the publication : " + bookings.get(i).getPublication().getTitle() + " by user " + member.getAccountOwnerUsername() + " with id: " + member.getId());
                LOGGER.info("Pub Id: " + bookings.get(i).getPublication().getId() + " and getBookingId: " + bookings.get(i).getId() );
                position = proxy.countUserPosition( bookings.get(i).getPublication().getId(), bookings.get(i).getId() );
                LOGGER.info("Position n° : " + position);
                bookings.get(i).setPosition( position + 1 );
                bookingCopies.put(bookings.get(i), copies);
            }

            model.addAttribute("bookingCopies", bookingCopies);
        } catch( FeignException f) {
               LOGGER.error("Feign exception: " + f.contentUTF8());
        }

        Object cancellation = session.getAttribute("cancellation");
        model.addAttribute("cancellation", cancellation);
        session.removeAttribute("cancellation");

        return USER_BOOKINGS_VIEW;
    }



    @GetMapping("/Booking/delete")
    public String delete( @RequestParam int idb, HttpSession session ) {
        Object token = session.getAttribute("token");
        proxy.updateBookingStatus( idb, BookingStatus.CANCELLED, token );
        session.setAttribute("cancellation", true);
        return USER_BOOKINGS_PAGE;

    }
}
