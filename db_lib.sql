-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: db_library
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accounts` (
  `user_type` varchar(31) NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `account_owner_email` varchar(255) NOT NULL,
  `account_owner_firstname` varchar(255) NOT NULL,
  `account_owner_lastname` varchar(255) NOT NULL,
  `account_owner_phone_nb` varchar(255) NOT NULL,
  `active_account` bit(1) NOT NULL,
  `registration_date` date NOT NULL,
  `role` varchar(255) NOT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `nb_of_currents_loans` int DEFAULT NULL,
  `account_owner_username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES ('Member',1,'lam99@hotmail.fr','Maryam','Lau','0388324567',_binary '','2019-02-10','USER','MARAS1-3',2,'maryam'),('Member',2,'lam99@hotmail.fr','Sophie','Goodman','0388324567',_binary '','2019-02-10','USER','SOPHIE1-2',3,'sophie'),('Employee',3,'admin@gmail.com','admin','admin','0388324567',_binary '','2019-02-10','ADMIN',NULL,0,'admin'),('Member',10,'amina@gmail.com','Amina','Lakhdar','0388324567',_binary '','2019-02-10','USER','AMINA1-4',3,'amina'),('Member',11,'ali@gmail.com','Ali','Bouama','0388324567',_binary '','2019-02-10','USER','ALI-15',1,'ali'),('Member',12,'lam99@hotmail.fr','Anne','Schmit','0388324567',_binary '','2019-02-10','USER','Anne-1-6',2,'anne'),('Member',13,'leo@gmail.com','Leo','Leo','0388324567',_binary '','2019-02-10','USER','LEO-1-7',1,'leo'),('Member',14,'lam99@hotmail.fr','Samy','Samy','0388324567',_binary '','2019-02-10','USER','SAMY-1-8',2,'samy');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors`
--

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` VALUES (1,NULL,'Victor','Hugo'),(2,NULL,'Guy','De Maupassant'),(3,NULL,'Gustave','Flaubert'),(4,'Molière','Jean-Baptiste','Oquelin');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookings`
--

DROP TABLE IF EXISTS `bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bookings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `booking_date` date DEFAULT NULL,
  `publication_fk` int NOT NULL,
  `member_fk` int NOT NULL,
  `booking_status` varchar(255) NOT NULL,
  `mail_date` date DEFAULT NULL,
  `copy_fk` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bookings_publication_fk_idx` (`publication_fk`),
  KEY `bookings_member_fk_idx` (`member_fk`),
  KEY `FKrxtniavj8tu948wqv7ebe4w87` (`copy_fk`),
  CONSTRAINT `bookings_member_fk` FOREIGN KEY (`member_fk`) REFERENCES `accounts` (`id`),
  CONSTRAINT `bookings_publication_fk` FOREIGN KEY (`publication_fk`) REFERENCES `publications` (`id`),
  CONSTRAINT `FK7ee48lgqjdbmu5ad7vsox3w08` FOREIGN KEY (`publication_fk`) REFERENCES `publications` (`id`),
  CONSTRAINT `FKg8hnaoacvvgevgv4rx9hrl5us` FOREIGN KEY (`member_fk`) REFERENCES `accounts` (`id`),
  CONSTRAINT `FKrxtniavj8tu948wqv7ebe4w87` FOREIGN KEY (`copy_fk`) REFERENCES `copies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookings`
--

LOCK TABLES `bookings` WRITE;
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;
INSERT INTO `bookings` VALUES (28,'2020-12-10',2,12,'CANCELLED','2021-02-05',5),(29,'2020-12-11',2,11,'CANCELLED','2021-02-05',5),(30,'2020-12-12',2,13,'CANCELLED','2021-02-05',5),(31,'2020-12-13',2,14,'FINISHED','2021-02-22',5),(33,'2020-12-11',9,2,'CANCELLED','2021-02-18',19),(34,'2020-12-14',9,14,'FINISHED','2021-02-23',19),(36,'2021-02-20',6,14,'INPROGRESS',NULL,NULL),(37,'2021-02-20',2,10,'FINISHED','2021-02-24',5),(38,'2021-02-20',9,11,'INPROGRESS','2021-03-17',19),(39,'2021-02-20',2,2,'CANCELLED',NULL,NULL),(40,'2021-02-20',9,12,'INPROGRESS',NULL,NULL),(41,'2021-02-20',2,12,'INPROGRESS',NULL,NULL),(42,'2021-03-05',2,1,'CANCELLED',NULL,NULL),(43,'2021-03-05',2,2,'CANCELLED',NULL,NULL),(44,'2021-03-05',2,2,'INPROGRESS',NULL,NULL),(45,'2021-03-15',9,1,'INPROGRESS',NULL,NULL),(46,'2021-03-15',3,10,'INPROGRESS',NULL,NULL),(47,'2021-03-15',9,2,'INPROGRESS',NULL,NULL);
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `copies`
--

DROP TABLE IF EXISTS `copies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `copies` (
  `id` int NOT NULL AUTO_INCREMENT,
  `available` bit(1) NOT NULL,
  `barcode` varchar(255) NOT NULL,
  `return_date` date DEFAULT NULL,
  `library_fk` int NOT NULL,
  `publication_fk` int NOT NULL,
  `booked` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `copies_library_fk_idx` (`library_fk`),
  KEY `copies_publication_fk_idx` (`publication_fk`),
  CONSTRAINT `copies_library_fk` FOREIGN KEY (`library_fk`) REFERENCES `libraries` (`id`),
  CONSTRAINT `copies_publication_fk` FOREIGN KEY (`publication_fk`) REFERENCES `publications` (`id`),
  CONSTRAINT `FK23b2oa5wh62flpuxf7a9tm8kh` FOREIGN KEY (`publication_fk`) REFERENCES `publications` (`id`),
  CONSTRAINT `FKtdjnluyacapwwqv1ri621xbln` FOREIGN KEY (`library_fk`) REFERENCES `libraries` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `copies`
--

LOCK TABLES `copies` WRITE;
/*!40000 ALTER TABLE `copies` DISABLE KEYS */;
INSERT INTO `copies` VALUES (1,_binary '','RVICTM-LD-1-1',NULL,1,1,_binary '\0'),(2,_binary '','RVICTM-LD-1-2',NULL,1,1,_binary '\0'),(3,_binary '','RVICTM-LD-2-1',NULL,2,1,_binary '\0'),(4,_binary '\0','PVICTO-GA-2-1','2021-04-02',2,2,_binary '\0'),(5,_binary '\0','PVICTO-GA-2-2','2021-04-02',3,2,_binary '\0'),(6,_binary '\0','RGUYDB-BE-2-1','2021-03-14',2,3,_binary '\0'),(7,_binary '\0','RGUYDB-BE-3-1','2021-04-02',3,3,_binary '\0'),(8,_binary '\0','TMOLIM-NA-1-1','2021-04-30',1,4,_binary '\0'),(9,_binary '\0','TMOLIM-NA-2-1','2021-04-02',2,4,_binary '\0'),(10,_binary '\0','TMOLIM-NA-2-2','2021-04-02',2,4,_binary '\0'),(11,_binary '\0','TMOLIM-NA-3-1','2021-04-02',3,4,_binary '\0'),(12,_binary '','GEO-01-20-2-1',NULL,2,5,_binary '\0'),(13,_binary '\0','GEO-01-20-2-2','2021-04-02',2,6,_binary '\0'),(14,_binary '\0','OBS-05-19-2-1','2021-04-12',2,7,_binary '\0'),(15,_binary '','MOND-01-20-1-1',NULL,2,8,_binary '\0'),(16,_binary '','MOND-01-20-2-1',NULL,2,8,_binary '\0'),(17,_binary '\0','MOND-01-20-2-2','2021-04-12',2,8,_binary '\0'),(18,_binary '\0','MOND-02-20-3-1','2021-04-02',2,9,_binary '\0'),(19,_binary '\0','MOND-02-20-3-2','2021-04-02',1,9,_binary ''),(20,_binary '','RVICTM-BE-3-1',NULL,3,10,_binary '\0'),(21,_binary '\0','RVICTM-BE-2-1','2021-03-24',2,10,_binary '\0'),(22,_binary '','RVICTM-NA-2-1',NULL,2,11,_binary '\0'),(23,_binary '\0','RVICTM-NA-2-1','2021-03-24',2,11,_binary '\0'),(24,_binary '','RVICTM-NA-2-1',NULL,3,11,_binary '\0'),(25,_binary '','RVICTM-NA-2-1',NULL,3,11,_binary '\0');
/*!40000 ALTER TABLE `copies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `editors`
--

DROP TABLE IF EXISTS `editors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `editors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `editors`
--

LOCK TABLES `editors` WRITE;
/*!40000 ALTER TABLE `editors` DISABLE KEYS */;
INSERT INTO `editors` VALUES (1,'Ldp Jeunesse'),(2,'Gallimard'),(3,'Belin Éducation'),(4,'Nathan');
/*!40000 ALTER TABLE `editors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `libraries`
--

DROP TABLE IF EXISTS `libraries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `libraries` (
  `id` int NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `libraries`
--

LOCK TABLES `libraries` WRITE;
/*!40000 ALTER TABLE `libraries` DISABLE KEYS */;
INSERT INTO `libraries` VALUES (1,'10 rue des écrivains','Le coin des lecteurs'),(2,'5 avenue de normandie','BMS'),(3,'2 rue Charles Péguy','Librairie Kleber');
/*!40000 ALTER TABLE `libraries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loans`
--

DROP TABLE IF EXISTS `loans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loans` (
  `id` int NOT NULL AUTO_INCREMENT,
  `extented` int NOT NULL,
  `loan_date` date NOT NULL,
  `loan_status` varchar(255) NOT NULL,
  `reminder_nb` int NOT NULL,
  `return_date` date NOT NULL,
  `copy_fk` int NOT NULL,
  `member_fk` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `loans_copy_fk_idx` (`copy_fk`),
  KEY `loans_member_fk_idx` (`member_fk`),
  CONSTRAINT `FKksilg4jm8e2gbpu70rhw5754i` FOREIGN KEY (`copy_fk`) REFERENCES `copies` (`id`),
  CONSTRAINT `FKlusyhxhdljg6nvb9eite87ayg` FOREIGN KEY (`member_fk`) REFERENCES `accounts` (`id`),
  CONSTRAINT `loans_copy_fk` FOREIGN KEY (`copy_fk`) REFERENCES `copies` (`id`),
  CONSTRAINT `loans_member_fk` FOREIGN KEY (`member_fk`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loans`
--

LOCK TABLES `loans` WRITE;
/*!40000 ALTER TABLE `loans` DISABLE KEYS */;
INSERT INTO `loans` VALUES (1,0,'2021-02-14','INPROGRESS',24,'2021-03-14',6,1),(2,0,'2020-11-21','FINISHED',18,'2020-12-25',15,1),(3,0,'2020-09-01','FINISHED',0,'2020-09-28',12,1),(4,1,'2020-09-01','FINISHED',0,'2020-12-01',19,1),(5,1,'2021-03-01','INPROGRESS',18,'2021-04-30',8,1),(6,0,'2020-11-01','FINISHED',18,'2020-12-10',9,1),(7,0,'2020-09-01','FINISHED',0,'2020-09-28',17,1),(8,0,'2021-03-01','INPROGRESS',18,'2021-04-02',7,2),(15,0,'2021-03-01','INPROGRESS',18,'2021-04-02',18,10),(16,0,'2021-03-01','INPROGRESS',18,'2021-04-02',4,11),(17,0,'2020-12-08','FINISHED',13,'2021-01-05',5,2),(18,0,'2021-03-01','INPROGRESS',18,'2021-04-02',13,2),(19,0,'2020-12-09','FINISHED',18,'2021-01-06',10,13),(20,0,'2021-03-01','INPROGRESS',18,'2021-04-02',11,14),(22,0,'2021-02-20','FINISHED',0,'2021-03-20',5,14),(23,0,'2021-03-01','FINISHED',0,'2021-04-02',19,14),(24,0,'2021-03-01','INPROGRESS',0,'2021-04-02',10,14),(25,0,'2021-03-01','INPROGRESS',0,'2021-04-02',5,10),(26,0,'2021-03-01','INPROGRESS',0,'2021-04-02',9,10),(27,0,'2021-02-14','FINISHED',0,'2021-03-14',16,2),(28,0,'2021-02-24','INPROGRESS',0,'2021-03-24',21,12),(29,0,'2021-02-24','INPROGRESS',0,'2021-03-24',23,12),(30,0,'2021-03-15','FINISHED',0,'2021-04-12',17,13),(31,0,'2021-03-15','FINISHED',0,'2021-04-12',17,13),(32,0,'2021-03-15','FINISHED',0,'2021-04-12',17,13),(33,0,'2021-03-15','INPROGRESS',0,'2021-04-12',17,13),(34,0,'2021-03-15','INPROGRESS',0,'2021-04-12',14,2);
/*!40000 ALTER TABLE `loans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publications`
--

DROP TABLE IF EXISTS `publications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `publications` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `identification_nb` varchar(255) NOT NULL,
  `nb_of_available_copies` int NOT NULL,
  `nb_total_ofcopies` int NOT NULL,
  `publication_date` date DEFAULT NULL,
  `sub_category` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author_fk` int DEFAULT NULL,
  `edithor_fk` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `publications_author_fk_idx` (`author_fk`),
  KEY `publications_editor_fk_idx` (`edithor_fk`),
  CONSTRAINT `FK3ipkf9x8d2m2bqqua1e93wt02` FOREIGN KEY (`author_fk`) REFERENCES `authors` (`id`),
  CONSTRAINT `FKd74lo2ev6pepcc3pf1t6lvgct` FOREIGN KEY (`edithor_fk`) REFERENCES `editors` (`id`),
  CONSTRAINT `publications_author_fk` FOREIGN KEY (`author_fk`) REFERENCES `authors` (`id`),
  CONSTRAINT `publications_editor_fk` FOREIGN KEY (`edithor_fk`) REFERENCES `editors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publications`
--

LOCK TABLES `publications` WRITE;
/*!40000 ALTER TABLE `publications` DISABLE KEYS */;
INSERT INTO `publications` VALUES (1,'BOOK','2010008995',3,3,'2014-01-01','NOVEL','Les misérables',1,1),(2,'BOOK','2070321908',0,2,'2015-01-01','POETRY','Odes et Ballades',1,2),(3,'BOOK','2701151589',0,2,'2019-01-01','NOVEL','Bel-Ami',2,4),(4,'BOOK',' 978-2-09-188737-1',1,4,'2018-01-01','THEATER','Le médecin malgé lui',4,3),(5,'REVIEW','dsfdgh',1,1,'2020-01-01','SCIENTIST','Geo',NULL,NULL),(6,'REVIEW','ereterh',0,1,'2020-02-01','SCIENTIST','Geo',NULL,NULL),(7,'REVIEW','ereterh',0,1,'2020-01-01','NEWS','L\'Obs',NULL,NULL),(8,'NEWSPAPER','ereterh',2,3,'2020-01-01','NATIONAL','Le Monde',NULL,NULL),(9,'NEWSPAPER','ereterh',0,2,'2020-02-01','NATIONAL','Le Monde',NULL,NULL),(10,'BOOK','2010008995',1,2,'2014-01-01','NOVEL','Les misérables',1,3),(11,'BOOK','DEWDEDF',3,4,'2014-01-01','NOVEL','Les misérables',1,4);
/*!40000 ALTER TABLE `publications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'db_library'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-15 11:13:42
