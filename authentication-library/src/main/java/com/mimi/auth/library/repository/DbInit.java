package com.mimi.auth.library.repository;

import com.mimi.auth.library.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class DbInit implements CommandLineRunner {

    private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;

    public DbInit(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) throws Exception {

       /*User user = new User("samy", passwordEncoder.encode("samy"), "USER", "ACCESS_USER");

        this.userRepository.save(user);*/

    }

}
