-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: db_library_bis
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accounts` (
  `user_type` varchar(31) NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `account_owner_email` varchar(255) NOT NULL,
  `account_owner_firstname` varchar(255) NOT NULL,
  `account_owner_lastname` varchar(255) NOT NULL,
  `account_owner_phone_nb` varchar(255) NOT NULL,
  `active_account` bit(1) NOT NULL,
  `registration_date` date NOT NULL,
  `role` varchar(255) NOT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `nb_of_currents_loans` int DEFAULT NULL,
  `account_owner_username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES ('Member',1,'mimi@gmail.com','mimi','mimi','0388324567',_binary '','2019-02-10','USER','Mimi1-1',2,'mimi'),('Member',2,'sofie.a@gmail.com','Sopie','Goodman','0388324567',_binary '','2019-02-10','USER','SOPHIE1-2',1,'sophie'),('Member',3,'est@hotmail.fr','Maryam','Launois','0388324567',_binary '','2019-02-10','USER','VELSEST1-3',0,'maryam'),('Employee',4,'lib@gmail.com','esthelle','esthelle','0388324567',_binary '','2019-02-10','LIBRARY',NULL,0,'esthelle'),('Employee',5,'admin@gmail.com','admin','admin','0388324567',_binary '','2019-02-10','ADMIN',NULL,0,'admin');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors`
--

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` VALUES (1,NULL,'Victor','Hugo'),(2,NULL,'Guy','De Maupassant'),(3,NULL,'Gustave','Flaubert'),(4,'Molière','Jean-Baptiste','oquelin');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `copies`
--

DROP TABLE IF EXISTS `copies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `copies` (
  `id` int NOT NULL AUTO_INCREMENT,
  `available` bit(1) NOT NULL,
  `barcode` varchar(255) NOT NULL,
  `return_date` date DEFAULT NULL,
  `library_fk` int DEFAULT NULL,
  `publication_fk` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKtdjnluyacapwwqv1ri621xbln` (`library_fk`),
  KEY `FK23b2oa5wh62flpuxf7a9tm8kh` (`publication_fk`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `copies`
--

LOCK TABLES `copies` WRITE;
/*!40000 ALTER TABLE `copies` DISABLE KEYS */;
INSERT INTO `copies` VALUES (1,_binary '\0','RVICTM-LD-1-1',NULL,1,1),(2,_binary '\0','RVICTM-LD-1-2',NULL,1,1),(3,_binary '','RVICTM-LD-2-1','2020-04-03',2,1),(4,_binary '','PVICTO-GA-2-1','2020-04-20',2,2),(5,_binary '','PVICTO-GA-2-2','2020-05-29',3,2),(6,_binary '\0','RGUYDB-BE-2-1','2020-05-19',2,3),(7,_binary '\0','RGUYDB-BE-3-1',NULL,3,3),(8,_binary '\0','TMOLIM-NA-1-1','2020-05-28',1,4),(9,_binary '\0','TMOLIM-NA-2-1','2020-05-28',2,4),(10,_binary '','TMOLIM-NA-2-2',NULL,2,4),(11,_binary '','TMOLIM-NA-3-1','2020-07-07',3,4),(12,_binary '\0','GEO-01-20-2-1',NULL,2,5),(13,_binary '','GEO-01-20-2-2',NULL,2,6),(14,_binary '','OBS-05-19-2-1',NULL,2,7),(15,_binary '\0','MOND-01-20-1-1',NULL,2,8),(16,_binary '','MOND-01-20-2-1',NULL,2,8),(17,_binary '','MOND-01-20-2-2','2020-05-28',2,8),(18,_binary '','MOND-02-20-3-1',NULL,2,9),(19,_binary '\0','MOND-02-20-3-2','2020-06-24',1,9),(20,_binary '','RVICTM-BE-3-1',NULL,3,10),(21,_binary '','RVICTM-BE-2-1',NULL,2,10),(22,_binary '','RVICTM-NA-2-1',NULL,2,11);
/*!40000 ALTER TABLE `copies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `editors`
--

DROP TABLE IF EXISTS `editors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `editors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `editors`
--

LOCK TABLES `editors` WRITE;
/*!40000 ALTER TABLE `editors` DISABLE KEYS */;
INSERT INTO `editors` VALUES (1,'Ldp Jeunesse'),(2,'Gallimard'),(3,'Belin Éducation'),(4,'Nathan');
/*!40000 ALTER TABLE `editors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `libraries`
--

DROP TABLE IF EXISTS `libraries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `libraries` (
  `id` int NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `libraries`
--

LOCK TABLES `libraries` WRITE;
/*!40000 ALTER TABLE `libraries` DISABLE KEYS */;
INSERT INTO `libraries` VALUES (1,'10 rue des écrivains','Le coin des lecteurs'),(2,'5 avenue de normandie','BMS'),(3,'2 rue Charles Péguy','Librairie Kleber');
/*!40000 ALTER TABLE `libraries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loans`
--

DROP TABLE IF EXISTS `loans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loans` (
  `id` int NOT NULL AUTO_INCREMENT,
  `extented` int NOT NULL,
  `loan_date` date NOT NULL,
  `loan_status` varchar(255) NOT NULL,
  `reminder_nb` int NOT NULL,
  `return_date` date NOT NULL,
  `copy_fk` int DEFAULT NULL,
  `member_fk` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKksilg4jm8e2gbpu70rhw5754i` (`copy_fk`),
  KEY `FKlusyhxhdljg6nvb9eite87ayg` (`member_fk`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loans`
--

LOCK TABLES `loans` WRITE;
/*!40000 ALTER TABLE `loans` DISABLE KEYS */;
INSERT INTO `loans` VALUES (1,1,'2015-02-20','FINISHED',1,'2020-04-15',1,1),(2,1,'2019-01-20','FINISHED',1,'2020-03-19',2,1),(3,1,'2020-03-22','INPROGRESS',3,'2020-05-19',6,2),(4,1,'2020-02-03','INPROGRESS',3,'2020-04-03',3,1),(5,1,'2020-02-20','INPROGRESS',3,'2020-04-20',4,1),(6,1,'2020-04-14','INPROGRESS',0,'2020-05-14',11,1),(7,0,'2020-04-30','FINISHED',0,'2020-05-28',15,2),(8,0,'2020-04-30','FINISHED',0,'2020-05-28',12,2),(9,1,'2020-04-30','INPROGRESS',0,'2020-06-24',19,2),(10,0,'2020-04-30','INPROGRESS',0,'2020-05-28',8,2),(11,0,'2020-04-30','INPROGRESS',0,'2020-05-28',9,2),(12,0,'2020-04-30','INPROGRESS',0,'2020-05-28',17,2),(13,0,'2020-05-01','INPROGRESS',0,'2020-05-29',5,2);
/*!40000 ALTER TABLE `loans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publications`
--

DROP TABLE IF EXISTS `publications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `publications` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `identification_nb` varchar(255) NOT NULL,
  `nb_of_available_copies` int NOT NULL,
  `nb_total_ofcopies` int NOT NULL,
  `publication_date` date DEFAULT NULL,
  `sub_category` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author_fk` int DEFAULT NULL,
  `edithor_fk` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3ipkf9x8d2m2bqqua1e93wt02` (`author_fk`),
  KEY `FKd74lo2ev6pepcc3pf1t6lvgct` (`edithor_fk`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publications`
--

LOCK TABLES `publications` WRITE;
/*!40000 ALTER TABLE `publications` DISABLE KEYS */;
INSERT INTO `publications` VALUES (1,'BOOK','2010008995',1,3,'2014-01-01','NOVEL','Les misérables',1,1),(2,'BOOK','2070321908',2,2,'2015-01-01','POETRY','Odes et Ballades',1,2),(3,'BOOK','2701151589',1,2,'2019-01-01','NOVEL','Bel-Ami',2,3),(4,'BOOK',' 978-2-09-188737-1',1,3,'2018-01-01','THEATER','Le médecin malgé lui',4,4),(5,'REVIEW','dsfdgh',1,1,'2020-01-01','SCIENTIST','Geo',NULL,NULL),(6,'REVIEW','ereterh',1,1,'2020-01-01','SCIENTIST','Geo',NULL,NULL),(7,'REVIEW','ereterh',1,1,'2020-01-01','NEWS','L\'Obs',NULL,NULL),(8,'NEWSPAPER','ereterh',2,3,'2020-01-01','NATIONAL','Le Monde',NULL,NULL),(9,'NEWSPAPER','ereterh',1,1,'2020-01-01','NATIONAL','Le Monde',NULL,NULL),(10,'BOOK','2010008995',2,2,'2014-01-01','NOVEL','Les misérables',1,3),(11,'BOOK','DEWDEDF',2,4,'2014-01-01','NOVEL','Les misérables',1,4);
/*!40000 ALTER TABLE `publications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'db_library_bis'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-15 11:14:32
