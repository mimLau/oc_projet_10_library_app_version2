# Système de gestion de bibliothèques.

  
## Table of contents 
* [Informations générales](#général) 
* [Technologies utilisées](#technologies) 
* [configuration de la base de données](#bdd) 
* [Déploiement des modules](#déploiement) 
* [Schéma de l'architecture Kafka](#kafka) 


## Informations générales :  

### Nom de l'application  

Bibliothèques municipales de Strasbourg

### Description  

Système de gestion centralisée des bibliothèques d'une ville. Mise en place d'une interface utilisateur qui permet la recherche d'ouvrages dans les différentes bibliothèques de la ville. Les utilisateurs membre pourront consulter et prolonger leur prêt.


Le projet est constitué :

- d'un webservice Rest : **rest-api-library**, fournisseur de ressources récupérées depuis la base de données **db_library**.

- d'une application web: **client-library**, présentant une interface utilisateur. Elle communique avec le web service pour récupérer les ressources nécessaires à l'affichage des données.

- d'un serveur d'authentification: **authentication-library**, qui permet de s'assurer de l'intégrité de l'utilisateur qui se connecte. Il l'autorise ou non à accéder certaines données. Il communique avec le client web ainsi que le webservice ainsi qu'avec la base de données **db_auth_lib**.

- d'un service d'envoi d'e-mails : **mail-service-library**, pour gérer l'envoie automatique d'emails aux utilisateurs ayant des prêts en retard. Il communique avec le web service par le biais d'une communication de type push, en utilisant des messages Kafka. Ce service récupère des données en lecture seule à partir de la base de données MYSQL slave. (voir partie configuration de la base de données)

 Ces 4 modules communiquent entre eux par le biais du webservice Rest.


## Développement : 

* Ide: IntelliJ
* Java 13.0.1.  
* Maven 4.0.0 
* Springboot 2.2.5.RELEASE 
* Spring security
* Spring MVC
* Mysql
* Serveur Apache Kafka


## Configuration de la base de données : 

Base de données master:

* Type: Mysql
* N° du port 3306 
* Identifiant : root 
* Mot de passe : root 

Base de données slave:

* Type: Mysql
* N° du port 3307
* Identifiant : root 
* Mot de passe : root 


Le projet présente les schémas suivant : 

### Schéma db_auth_lib

Contient la table user avec les identifiants et les mots de passe ainsi que les rôles et les permissions des utilisateurs. Cette table est utilisée par le serveur d'authentification. Un jeu de données est fourni dans le fichier **db_auth_lib.sql**. (https://gitlab.com/mimLau/oc_projet_10_library_app_version2/-/blob/master/db_auth_lib.sql)

### Schéma d_library

Contient toutes les tables permetant la gestion des bibliothèqes ainsi que leurs ouvrages. Ces tables seront consommées par le webService. Un jeu de données est fourni dans le fichier **db_lib.sql**. (https://gitlab.com/mimLau/oc_projet_10_library_app_version2/-/blob/master/db_lib.sql)


## Déploiement de l'application : 


### Récupération et import du projet :

Cloner le projet vers un repertoire de votre choix à l'aide de la commande suivante :

**git clone https://gitlab.com/mimLau/oc_projet_10_library_app_version2.git**

Dans votre IDE, choisissez dans un premier temps le jdk qui sera utilisé pour lancer le projet. Dans notre cas, il s'agira du jdk 13.0.1.
Importer ensuite le projet cloné à partir d'une source existante et en cliquant bien sur Maven.
Ensuite, se rendre dans **project structure** puis **module** et à l'aide du **+** importer les modules suivant un à un (choisir Maven comme source externe).

- rest-api-library
- authentication-library 
- client-library
- mail-service-library


### Lancement du projet :

Pour lancer le projet, excécuter les modules dans l'ordre suivant sur leur port respectif:

- microservice-library: port 8080
- authentication-library: port 9090
- client-library: port 8081
- batch-library: port 8082


Spring boot contenant un serveur Tomcat embarqué, il ne sera pas necessaire de configurer un serveur lors du déploiement des différents modules.

Dans chacun des modules, se trouve un fichier **applications.properties** dans lequel sont stockées toutes les donées de configuration. Vous pouvez modifier les différents ports s'ils ne sont pas libres.


#### Webservice Rest :

Il récupère toutes les ressources de la BDD **db_library** et les envoie à ses differents clients qui sont:

- le serveur d'authentification
- l'application web
- le service d'envoi d'e-mails

Chaque requête envoyée par les clients contient un token. En fonction des rôles ou permissions qui se trouvent dans le token, la requête sera acceptée ou non par le webservice.

On peut y accéder via l'url suivante: **localhost:8080/**


#### Serveur auhtentification :

Celui ci est configuré sur le port 9090, et récupère les crédentials des utilisateurs à partir de la BDD **db_auth_library**.

L'application web envoie au serveur d'authentification les identifiants de l'utilisateur qui souhaite se connecter, ce dernier va vérifier dans sa BDD les identifiants de l'utilisateur. Si l'utilisateur existe, le serveur envoie un token à l'application web avec le rôle et les permissions de ce dernier.


#### Application web :

On accède à l'application web avec l'url suivante : **localhost:8083/**. 
Les identifiants et des mots de passe des utilisateurs sont les suivants :

- maryam maryam
- sophie sophie
- admin admin


#### Service d'envoi d'e-mails :

Il est programmé pour lancer automatiquement des emails tous les jours aux utilisateurs ayant du retard dans leur prêt. Pour cela, il communique avec la base de données slave, pour récupérer la listes des utilisateurs ayant du retard.
Il reçoit des message kafka envoyés par le webservice qui l'informe qu'une copie est disponible. Le service pourra de ce fait, envoyer un e-mail à l'utilisareur qui a fait la réservation.


## Kafka : Schéma de l'architecture


![Kafka architecture](Kafka Schema.PNG "Kafka architecture")

