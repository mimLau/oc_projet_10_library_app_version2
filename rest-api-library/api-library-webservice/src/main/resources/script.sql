create table accounts
(
    user_type               varchar(31)  not null,
    id                      int auto_increment
        primary key,
    account_owner_email     varchar(255) not null,
    account_owner_firstname varchar(255) not null,
    account_owner_lastname  varchar(255) not null,
    account_owner_phone_nb  varchar(255) not null,
    active_account          bit          not null,
    registration_date       date         not null,
    role                    varchar(255) not null,
    barcode                 varchar(255) null,
    nb_of_currents_loans    int          null,
    account_owner_username  varchar(255) not null
);

create table authors
(
    id        int auto_increment
        primary key,
    alias     varchar(255) null,
    firstname varchar(255) not null,
    lastname  varchar(255) not null
);

create table editors
(
    id   int auto_increment
        primary key,
    name varchar(255) not null
);

create table libraries
(
    id      int auto_increment
        primary key,
    address varchar(255) not null,
    name    varchar(255) not null
);

create table publications
(
    id                     int auto_increment
        primary key,
    category               varchar(255) not null,
    identification_nb      varchar(255) not null,
    nb_of_available_copies int          not null,
    nb_total_ofcopies      int          not null,
    publication_date       date         null,
    sub_category           varchar(255) not null,
    title                  varchar(255) not null,
    author_fk              int          null,
    edithor_fk             int          null,
    constraint FK3ipkf9x8d2m2bqqua1e93wt02
        foreign key (author_fk) references authors (id),
    constraint FKd74lo2ev6pepcc3pf1t6lvgct
        foreign key (edithor_fk) references editors (id),
    constraint publications_author_fk
        foreign key (author_fk) references authors (id),
    constraint publications_editor_fk
        foreign key (edithor_fk) references editors (id)
);

create table copies
(
    id             int auto_increment
        primary key,
    available      bit          not null,
    barcode        varchar(255) not null,
    return_date    date         null,
    library_fk     int          not null,
    publication_fk int          not null,
    booked         bit          not null,
    constraint FK23b2oa5wh62flpuxf7a9tm8kh
        foreign key (publication_fk) references publications (id),
    constraint FKtdjnluyacapwwqv1ri621xbln
        foreign key (library_fk) references libraries (id),
    constraint copies_library_fk
        foreign key (library_fk) references libraries (id),
    constraint copies_publication_fk
        foreign key (publication_fk) references publications (id)
);

create table bookings
(
    id             int auto_increment
        primary key,
    booking_date   date         null,
    publication_fk int          not null,
    member_fk      int          not null,
    booking_status varchar(255) not null,
    mail_date      date         null,
    copy_fk        int          null,
    constraint FK7ee48lgqjdbmu5ad7vsox3w08
        foreign key (publication_fk) references publications (id),
    constraint FKg8hnaoacvvgevgv4rx9hrl5us
        foreign key (member_fk) references accounts (id),
    constraint FKrxtniavj8tu948wqv7ebe4w87
        foreign key (copy_fk) references copies (id),
    constraint bookings_member_fk
        foreign key (member_fk) references accounts (id),
    constraint bookings_publication_fk
        foreign key (publication_fk) references publications (id)
);

create index bookings_member_fk_idx
    on bookings (member_fk);

create index bookings_publication_fk_idx
    on bookings (publication_fk);

create index copies_library_fk_idx
    on copies (library_fk);

create index copies_publication_fk_idx
    on copies (publication_fk);

create table loans
(
    id          int auto_increment
        primary key,
    extented    int          not null,
    loan_date   date         not null,
    loan_status varchar(255) not null,
    reminder_nb int          not null,
    return_date date         not null,
    copy_fk     int          not null,
    member_fk   int          not null,
    constraint FKksilg4jm8e2gbpu70rhw5754i
        foreign key (copy_fk) references copies (id),
    constraint FKlusyhxhdljg6nvb9eite87ayg
        foreign key (member_fk) references accounts (id),
    constraint loans_copy_fk
        foreign key (copy_fk) references copies (id),
    constraint loans_member_fk
        foreign key (member_fk) references accounts (id)
);

create index loans_copy_fk_idx
    on loans (copy_fk);

create index loans_member_fk_idx
    on loans (member_fk);

create index publications_author_fk_idx
    on publications (author_fk);

create index publications_editor_fk_idx
    on publications (edithor_fk);