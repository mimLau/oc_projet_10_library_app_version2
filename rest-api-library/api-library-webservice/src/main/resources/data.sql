SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE db_library_test.accounts;
TRUNCATE db_library_test.authors;
TRUNCATE db_library_test.libraries;
TRUNCATE db_library_test.editors;
TRUNCATE db_library_test.publications;
TRUNCATE db_library_test.copies;
TRUNCATE db_library_test.loans;
TRUNCATE db_library_test.bookings;

SET FOREIGN_KEY_CHECKS = 1;


INSERT INTO db_library_test.accounts (user_type, id, account_owner_email, account_owner_firstname, account_owner_lastname, account_owner_phone_nb, active_account, registration_date, role, barcode, nb_of_currents_loans, account_owner_username) VALUES ('Member', 1, 'lam99@hotmail.fr', 'Maryam', 'Lau', '0388324567', true, '2019-02-10', 'USER', 'MARAS1-3', 2, 'maryam');
INSERT INTO db_library_test.accounts (user_type, id, account_owner_email, account_owner_firstname, account_owner_lastname, account_owner_phone_nb, active_account, registration_date, role, barcode, nb_of_currents_loans, account_owner_username) VALUES ('Member', 2, 'lam99@hotmail.fr', 'Sophie', 'Goodman', '0388324567', true, '2019-02-10', 'USER', 'SOPHIE1-2', 5, 'sophie');
INSERT INTO db_library_test.accounts (user_type, id, account_owner_email, account_owner_firstname, account_owner_lastname, account_owner_phone_nb, active_account, registration_date, role, barcode, nb_of_currents_loans, account_owner_username) VALUES ('Employee', 3, 'admin@gmail.com', 'admin', 'admin', '0388324567', true, '2019-02-10', 'ADMIN', null, 0, 'admin');
INSERT INTO db_library_test.accounts (user_type, id, account_owner_email, account_owner_firstname, account_owner_lastname, account_owner_phone_nb, active_account, registration_date, role, barcode, nb_of_currents_loans, account_owner_username) VALUES ('Member', 10, 'amina@gmail.com', 'Amina', 'Lakhdar', '0388324567', true, '2019-02-10', 'USER', 'AMINA1-4', 3, 'amina');
INSERT INTO db_library_test.accounts (user_type, id, account_owner_email, account_owner_firstname, account_owner_lastname, account_owner_phone_nb, active_account, registration_date, role, barcode, nb_of_currents_loans, account_owner_username) VALUES ('Member', 11, 'ali@gmail.com', 'Ali', 'Bouama', '0388324567', true, '2019-02-10', 'USER', 'ALI-15', 0, 'ali');
INSERT INTO db_library_test.accounts (user_type, id, account_owner_email, account_owner_firstname, account_owner_lastname, account_owner_phone_nb, active_account, registration_date, role, barcode, nb_of_currents_loans, account_owner_username) VALUES ('Member', 12, 'lam99@hotmail.fr', 'Anne', 'Schmit', '0388324567', true, '2019-02-10', 'USER', 'Anne-1-6', 2, 'anne');
INSERT INTO db_library_test.accounts (user_type, id, account_owner_email, account_owner_firstname, account_owner_lastname, account_owner_phone_nb, active_account, registration_date, role, barcode, nb_of_currents_loans, account_owner_username) VALUES ('Member', 13, 'leo@gmail.com', 'Leo', 'Leo', '0388324567', true, '2019-02-10', 'USER', 'LEO-1-7', 0, 'leo');
INSERT INTO db_library_test.accounts (user_type, id, account_owner_email, account_owner_firstname, account_owner_lastname, account_owner_phone_nb, active_account, registration_date, role, barcode, nb_of_currents_loans, account_owner_username) VALUES ('Member', 14, 'lam99@hotmail.fr', 'Samy', 'Samy', '0388324567', true, '2019-02-10', 'USER', 'SAMY-1-8', 3, 'samy');


INSERT INTO db_library_test.authors (id, alias, firstname, lastname) VALUES (1, null, 'Victor', 'Hugo');
INSERT INTO db_library_test.authors (id, alias, firstname, lastname) VALUES (2, null, 'Guy', 'De Maupassant');
INSERT INTO db_library_test.authors (id, alias, firstname, lastname) VALUES (3, null, 'Gustave', 'Flaubert');
INSERT INTO db_library_test.authors (id, alias, firstname, lastname) VALUES (4, 'Molière', 'Jean-Baptiste', 'Oquelin');

INSERT INTO db_library_test.libraries (id, address, name) VALUES (1, '10 rue des écrivains', 'Le coin des lecteurs');
INSERT INTO db_library_test.libraries (id, address, name) VALUES (2, '5 avenue de normandie', 'BMS');
INSERT INTO db_library_test.libraries (id, address, name) VALUES (3, '2 rue Charles Péguy', 'Librairie Kleber');

INSERT INTO db_library_test.editors (id, name) VALUES (1, 'Ldp Jeunesse');
INSERT INTO db_library_test.editors (id, name) VALUES (2, 'Gallimard');
INSERT INTO db_library_test.editors (id, name) VALUES (3, 'Belin Éducation');
INSERT INTO db_library_test.editors (id, name) VALUES (4, 'Nathan');

INSERT INTO db_library_test.publications (id, category, identification_nb, nb_of_available_copies, nb_total_ofcopies, publication_date, sub_category, title, author_fk, edithor_fk) VALUES (1, 'BOOK', '2010008995', 2, 3, '2014-01-01', 'NOVEL', 'Les misérables', 1, 1);
INSERT INTO db_library_test.publications (id, category, identification_nb, nb_of_available_copies, nb_total_ofcopies, publication_date, sub_category, title, author_fk, edithor_fk) VALUES (2, 'BOOK', '2070321908', 0, 2, '2015-01-01', 'POETRY', 'Odes et Ballades', 1, 2);
INSERT INTO db_library_test.publications (id, category, identification_nb, nb_of_available_copies, nb_total_ofcopies, publication_date, sub_category, title, author_fk, edithor_fk) VALUES (3, 'BOOK', '2701151589', 0, 2, '2019-01-01', 'NOVEL', 'Bel-Ami', 2, 4);
INSERT INTO db_library_test.publications (id, category, identification_nb, nb_of_available_copies, nb_total_ofcopies, publication_date, sub_category, title, author_fk, edithor_fk) VALUES (4, 'BOOK', ' 978-2-09-188737-1', 1, 4, '2018-01-01', 'THEATER', 'Le médecin malgé lui', 4, 3);
INSERT INTO db_library_test.publications (id, category, identification_nb, nb_of_available_copies, nb_total_ofcopies, publication_date, sub_category, title, author_fk, edithor_fk) VALUES (5, 'REVIEW', 'dsfdgh', 0, 1, '2020-01-01', 'SCIENTIST', 'Geo', null, null);
INSERT INTO db_library_test.publications (id, category, identification_nb, nb_of_available_copies, nb_total_ofcopies, publication_date, sub_category, title, author_fk, edithor_fk) VALUES (6, 'REVIEW', 'ereterh', 0, 1, '2020-02-01', 'SCIENTIST', 'Geo', null, null);
INSERT INTO db_library_test.publications (id, category, identification_nb, nb_of_available_copies, nb_total_ofcopies, publication_date, sub_category, title, author_fk, edithor_fk) VALUES (7, 'REVIEW', 'ereterh', 1, 1, '2020-01-01', 'NEWS', 'L''Obs', null, null);
INSERT INTO db_library_test.publications (id, category, identification_nb, nb_of_available_copies, nb_total_ofcopies, publication_date, sub_category, title, author_fk, edithor_fk) VALUES (8, 'NEWSPAPER', 'ereterh', 2, 3, '2020-01-01', 'NATIONAL', 'Le Monde', null, null);
INSERT INTO db_library_test.publications (id, category, identification_nb, nb_of_available_copies, nb_total_ofcopies, publication_date, sub_category, title, author_fk, edithor_fk) VALUES (9, 'NEWSPAPER', 'ereterh', 0, 2, '2020-02-01', 'NATIONAL', 'Le Monde', null, null);
INSERT INTO db_library_test.publications (id, category, identification_nb, nb_of_available_copies, nb_total_ofcopies, publication_date, sub_category, title, author_fk, edithor_fk) VALUES (10, 'BOOK', '2010008995', 1, 2, '2014-01-01', 'NOVEL', 'Les misérables', 1, 3);
INSERT INTO db_library_test.publications (id, category, identification_nb, nb_of_available_copies, nb_total_ofcopies, publication_date, sub_category, title, author_fk, edithor_fk) VALUES (11, 'BOOK', 'DEWDEDF', 3, 4, '2014-01-01', 'NOVEL', 'Les misérables', 1, 4);


INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (1, false, 'RVICTM-LD-1-1', '2021-04-02', 1, 1, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (2, true, 'RVICTM-LD-1-2', null, 1, 1, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (3, true, 'RVICTM-LD-2-1', null, 2, 1, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (4, false, 'PVICTO-GA-2-1', '2021-01-05', 2, 2, true);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (5, false, 'PVICTO-GA-2-2', '2021-04-02', 3, 2, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (6, false, 'RGUYDB-BE-2-1', '2021-03-14', 2, 3, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (7, false, 'RGUYDB-BE-3-1', '2021-04-02', 3, 3, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (8, false, 'TMOLIM-NA-1-1', '2021-04-02', 1, 4, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (9, false, 'TMOLIM-NA-2-1', '2021-04-02', 2, 4, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (10, false, 'TMOLIM-NA-2-2', '2021-04-02', 2, 4, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (11, false, 'TMOLIM-NA-3-1', '2021-04-02', 3, 4, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (12, false, 'GEO-01-20-2-1', '2021-04-02', 2, 5, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (13, false, 'GEO-01-20-2-2', '2021-03-05', 2, 6, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (14, true, 'OBS-05-19-2-1', null, 2, 7, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (15, true, 'MOND-01-20-1-1', null, 2, 8, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (16, false, 'MOND-01-20-2-1', '2021-04-02', 2, 8, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (17, true, 'MOND-01-20-2-2', null, 2, 8, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (18, false, 'MOND-02-20-3-1', '2021-04-02', 2, 9, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (19, false, 'MOND-02-20-3-2', '2021-03-14', 1, 9, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (20, true, 'RVICTM-BE-3-1', null, 3, 10, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (21, false, 'RVICTM-BE-2-1', '2021-04-02', 2, 10, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (22, true, 'RVICTM-NA-2-1', null, 2, 11, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (23, false, 'RVICTM-NA-2-1', '2021-04-02', 2, 11, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (24, true, 'RVICTM-NA-2-1', null, 3, 11, false);
INSERT INTO db_library_test.copies (id, available, barcode, return_date, library_fk, publication_fk, booked) VALUES (25, true, 'RVICTM-NA-2-1', null, 3, 11, false);




INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (1, 0, '2021-02-14', 'INPROGRESS', 24, '2021-03-14', 6, 1);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (2, 0, '2020-11-21', 'FINISHED', 18, '2020-12-25', 15, 1);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (3, 0, '2020-09-01', 'FINISHED', 0, '2020-09-28', 12, 1);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (4, 1, '2020-09-01', 'FINISHED', 0, '2020-12-01', 19, 1);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (5, 0, '2021-03-01', 'INPROGRESS', 18, '2021-04-02', 8, 1);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (6, 0, '2020-11-01', 'FINISHED', 18, '2020-12-10', 9, 1);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (7, 0, '2020-09-01', 'FINISHED', 0, '2020-09-28', 17, 1);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (8, 0, '2021-03-01', 'INPROGRESS', 18, '2021-04-02', 7, 2);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (15, 0, '2021-03-01', 'INPROGRESS', 18, '2021-04-02', 18, 10);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (16, 0, '2020-12-08', 'FINISHED', 18, '2021-01-05', 4, 11);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (17, 0, '2020-12-08', 'FINISHED', 13, '2021-01-05', 5, 2);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (18, 0, '2021-03-01', 'INPROGRESS', 18, '2021-04-02', 13, 2);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (19, 0, '2020-12-09', 'FINISHED', 18, '2021-01-06', 10, 13);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (20, 0, '2021-03-01', 'INPROGRESS', 18, '2021-04-02', 11, 14);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (22, 0, '2021-02-20', 'FINISHED', 0, '2021-03-20', 5, 14);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (23, 0, '2021-02-14', 'INPROGRESS', 0, '2021-03-14', 19, 14);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (24, 0, '2021-03-01', 'INPROGRESS', 0, '2021-04-02', 10, 14);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (25, 0, '2021-03-01', 'INPROGRESS', 0, '2021-04-02', 5, 10);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (26, 0, '2021-02-22', 'INPROGRESS', 0, '2021-04-02', 9, 10);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (27, 0, '2021-02-24', 'INPROGRESS', 0, '2021-04-02', 16, 2);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (28, 0, '2021-02-24', 'INPROGRESS', 0, '2021-04-02', 21, 12);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (29, 0, '2021-02-24', 'INPROGRESS', 0, '2021-04-02', 23, 12);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (30, 0, '2021-03-04', 'INPROGRESS', 0, '2021-04-02', 1, 2);
INSERT INTO db_library_test.loans (id, extented, loan_date, loan_status, reminder_nb, return_date, copy_fk, member_fk) VALUES (31, 0, '2021-03-04', 'INPROGRESS', 0, '2021-04-02', 12, 2);



INSERT INTO db_library_test.bookings (id, booking_date, publication_fk, member_fk, booking_status, mail_date, copy_fk) VALUES (28, '2020-12-10', 2, 12, 'CANCELLED', '2021-02-05', 5);
INSERT INTO db_library_test.bookings (id, booking_date, publication_fk, member_fk, booking_status, mail_date, copy_fk) VALUES (29, '2020-12-11', 2, 11, 'CANCELLED', '2021-02-05', 5);
INSERT INTO db_library_test.bookings (id, booking_date, publication_fk, member_fk, booking_status, mail_date, copy_fk) VALUES (30, '2020-12-12', 2, 13, 'CANCELLED', '2021-02-05', 5);
INSERT INTO db_library_test.bookings (id, booking_date, publication_fk, member_fk, booking_status, mail_date, copy_fk) VALUES (31, '2020-12-13', 2, 14, 'FINISHED', '2021-02-22', 5);
INSERT INTO db_library_test.bookings (id, booking_date, publication_fk, member_fk, booking_status, mail_date, copy_fk) VALUES (33, '2020-12-11', 9, 2, 'CANCELLED', '2021-02-18', 19);
INSERT INTO db_library_test.bookings (id, booking_date, publication_fk, member_fk, booking_status, mail_date, copy_fk) VALUES (34, '2020-12-14', 9, 14, 'FINISHED', '2021-02-23', 19);
INSERT INTO db_library_test.bookings (id, booking_date, publication_fk, member_fk, booking_status, mail_date, copy_fk) VALUES (36, '2021-02-20', 6, 14, 'INPROGRESS', null, null);
INSERT INTO db_library_test.bookings (id, booking_date, publication_fk, member_fk, booking_status, mail_date, copy_fk) VALUES (37, '2021-02-20', 2, 10, 'FINISHED', '2021-02-24', 5);
INSERT INTO db_library_test.bookings (id, booking_date, publication_fk, member_fk, booking_status, mail_date, copy_fk) VALUES (38, '2021-02-20', 9, 11, 'INPROGRESS', null, null);
INSERT INTO db_library_test.bookings (id, booking_date, publication_fk, member_fk, booking_status, mail_date, copy_fk) VALUES (39, '2021-02-20', 2, 1, 'INPROGRESS', '2021-04-03', 4);
INSERT INTO db_library_test.bookings (id, booking_date, publication_fk, member_fk, booking_status, mail_date, copy_fk) VALUES (40, '2021-02-20', 9, 12, 'INPROGRESS', null, null);
INSERT INTO db_library_test.bookings (id, booking_date, publication_fk, member_fk, booking_status, mail_date, copy_fk) VALUES (41, '2021-02-20', 2, 12, 'INPROGRESS', null, null);