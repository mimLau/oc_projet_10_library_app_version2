TRUNCATE db_library_test.accounts CASCADE;
TRUNCATE db_library_test.authors CASCADE;
TRUNCATE db_library_test.libraries CASCADE;
TRUNCATE db_library_test.editors CASCADE;
TRUNCATE db_library_test.publications CASCADE;
TRUNCATE db_library_test.copies CASCADE;
TRUNCATE db_library_test.loans CASCADE;
TRUNCATE db_library_test.bookings CASCADE;