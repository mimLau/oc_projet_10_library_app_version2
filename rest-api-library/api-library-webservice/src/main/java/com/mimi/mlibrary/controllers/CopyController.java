package com.mimi.mlibrary.controllers;

import com.mimi.mlibrary.exceptions.ResourceNotFoundException;
import com.mimi.mlibrary.model.dto.publication.CopyDto;
import com.mimi.mlibrary.service.contract.PublicationService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class CopyController {

    private PublicationService publicationService;

    public CopyController(PublicationService publicationService) {
        this.publicationService = publicationService;
    }

    @GetMapping( value = "/Copies/{id}" )
    public CopyDto getCoyById( @PathVariable int id ) {

        return publicationService.findCopyById( id ) ;
    }


    @GetMapping( value = "/Copies/delay" )
    public List<CopyDto> getAllByDelay(  ) {
        List<CopyDto> copyDtos =  publicationService.findAllCopyByDelay();
        if( copyDtos.isEmpty() ) throw new ResourceNotFoundException(  "Il n'y a aucun exemplaire dont la date de retour est passée." );

        return copyDtos;
    }

    @GetMapping( value = "/Copies/publication/{id}" )
    public List<CopyDto> getAllCopyByPublicationId( @PathVariable int id  ) {
        List<CopyDto> copyDtos =  publicationService.findAllCopyByPublicationId( id );
        if( copyDtos.isEmpty() ) throw new ResourceNotFoundException(  "Il n'y a aucun exemplaire qui correspond à cette ouvrage." );

        return copyDtos;
    }

    @GetMapping( value = "/Copies/publication", params = "pubId")
    public List<CopyDto> getAvailableCopiesByLibrary( @RequestParam int pubId  ) {
        List<CopyDto> copyDtos =  publicationService.findAvailableCopiesByLibrary( pubId );
        if( copyDtos.isEmpty() ) throw new ResourceNotFoundException(  "Il n'y a pas d'exemplaires de disponible de l'ouvrage recherché." );

        return copyDtos;
    }


}
