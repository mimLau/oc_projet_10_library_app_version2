package com.mimi.mlibrary.controllers;

import com.mimi.mlibrary.exceptions.ResourceNotFoundException;
import com.mimi.mlibrary.model.dto.publication.BookingDto;
import com.mimi.mlibrary.model.entity.publication.BookingJson;
import com.mimi.mlibrary.model.entity.publication.BookingStatus;
import com.mimi.mlibrary.service.contract.BookingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/Bookings")
public class BookingController {

    private BookingService bookingService;

    public BookingController( BookingService bookingService ) {
        this.bookingService = bookingService;
    }

    @GetMapping( "/{id}" )
    public BookingDto getBookingById( @PathVariable int id ) {
        return bookingService.getBookingById( id );
    }

    @GetMapping
    public List<BookingDto>  getAllBookings() {

        List<BookingDto> bookingDtos =  bookingService.getAllBookings() ;
        if( bookingDtos.isEmpty() ) throw new ResourceNotFoundException( "La liste des réservations est vide." );

        return bookingDtos ;
    }

    @PostMapping( consumes={"application/json"} )
    public ResponseEntity<?> addBooking( @RequestBody BookingJson bookingJson ) {

        //return new ResponseEntity<>(bookingService.save(bookingJson.getPublication(), bookingJson.getMember()), HttpStatus.CREATED);

        BookingDto addedBooking = bookingService.save( bookingJson.getPublication(), bookingJson.getMember() );
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand( addedBooking.getId() ).toUri();
        return ResponseEntity.created( location ).body(addedBooking);
    }

    @GetMapping( value = "/Publications/{id}/{status}" )
    public List<BookingDto>  getListBookingByPublicationId( @PathVariable int id, @PathVariable BookingStatus status ) {

       List<BookingDto> bookingDtos =  bookingService.getListBookingByPublicationId( id, status ) ;
       //if( bookingDtos.isEmpty() ) throw new ResourceNotFoundException( "Il n'y a pas de réservation pour cette publication." );

        return bookingDtos ;
    }

    @GetMapping( "/Members/{id}" )
    public List<BookingDto> getListBookingByUserId( @PathVariable int id ) {
        List<BookingDto> bookingDtos =  bookingService.getListBookingByUserId( id );
        if( bookingDtos.isEmpty() ) throw new ResourceNotFoundException( "Il n'y a pas de réservation d'enregistrée pour cet utilisateur." );

        return bookingDtos ;
    }

    @PutMapping( "/status/{bookingId}/{status}" )
    public void updateBookingStatus(@PathVariable int bookingId , @PathVariable BookingStatus status) {
        bookingService.updateBookingStatus( bookingId, status );
    }

    @GetMapping( "/count/{pId}/{bId}" )
    public Integer countUserPosition( @PathVariable int pId, @PathVariable int bId) {
      return bookingService.countUserPosition(pId, bId);
    }

    @GetMapping( "/copy/{copyId}/{status}" )
    public ResponseEntity<?>getBookingByCopyIdAndStatus( @PathVariable int copyId, @PathVariable BookingStatus status) {

        BookingDto bookingDto = bookingService.getBookingByCopyIdAndStatus(copyId, status);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand( bookingDto.getId() ).toUri();
        return ResponseEntity.created( location ).body(bookingDto);
    }
}
