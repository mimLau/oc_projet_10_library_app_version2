package com.mimi.mlibrary.service.impl;


import com.mimi.mlibrary.mapper.account.MemberMapper;
import com.mimi.mlibrary.mapper.loan.LoanMapper;
import com.mimi.mlibrary.mapper.publication.BookingMapper;
import com.mimi.mlibrary.mapper.publication.CopyMapper;
import com.mimi.mlibrary.mapper.publication.PublicationMapper;
import com.mimi.mlibrary.model.dto.account.MemberDto;
import com.mimi.mlibrary.model.dto.loan.LoanDto;
import com.mimi.mlibrary.model.dto.publication.BookingDto;
import com.mimi.mlibrary.model.dto.publication.CopyDto;
import com.mimi.mlibrary.model.dto.publication.PublicationDto;
import com.mimi.mlibrary.model.entity.loan.LoanStatus;
import com.mimi.mlibrary.model.entity.publication.Booking;
import com.mimi.mlibrary.model.entity.publication.BookingStatus;
import com.mimi.mlibrary.repository.account.MemberRepository;
import com.mimi.mlibrary.repository.loan.LoanRepository;
import com.mimi.mlibrary.repository.publication.BookingRepository;
import com.mimi.mlibrary.repository.publication.CopyRepository;
import com.mimi.mlibrary.repository.publication.PublicationRepository;
import com.mimi.mlibrary.service.contract.BookingService;
import com.mimi.mlibrary.service.exceptions.BadRequestException;
import com.mimi.mlibrary.service.exceptions.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BookingServiceImpl implements BookingService {

    private PublicationRepository publicationRepository;
    private MemberRepository memberRepository;
    private BookingRepository bookingRepository;
    private CopyRepository copyRepository;
    private LoanRepository loanRepository;

    public BookingServiceImpl(PublicationRepository publicationRepository, MemberRepository memberRepository, BookingRepository bookingRepository, CopyRepository copyRepository, LoanRepository loanRepository) {
        this.publicationRepository = publicationRepository;
        this.memberRepository = memberRepository;
        this.bookingRepository = bookingRepository;
        this.copyRepository = copyRepository;
        this.loanRepository = loanRepository;

    }

    @Override
    public BookingDto save(int publicationId, int memberId) {

        BookingDto bookingDto = new BookingDto();
        LocalDate today = LocalDate.now();
        List<CopyDto> publicationCopyDtos;
        List<LoanDto> loanDtos;
        BookingDto userBookingDto;
        BookingDto savedDtoBooking;
        List<BookingDto> publicationBookings;

        PublicationDto publicationDto = PublicationMapper.INSTANCE.toDto(publicationRepository.findPublicationById(publicationId).orElse(null));
        MemberDto memberDto = MemberMapper.INSTANCE.toDto(memberRepository.findById(memberId).orElse(null));


        // Before registering a booking of a publication to an user, verify if this both exists.

        if (publicationDto == null && memberDto == null)
            throw new ResourceNotFoundException("L'utilisateur et la publication recherchés n'existent pas.");
        else if (publicationDto == null)
            throw new ResourceNotFoundException("La publication recherchée n'existe pas.");
        else if (memberDto == null)
            throw new ResourceNotFoundException("L'utilisateur recherché n'existe pas.");


        // We retrieve all copies of the publication
        publicationCopyDtos = this.findAllCopyByPublicationId(publicationId);
        // Try to get all the copies of the publication that the user has borrowed, if there isn't any, loanDtos will be empty
        loanDtos = this.findUserLoan(publicationCopyDtos, memberId);
        // Try to get a booking done by the user for this publication, if there isn't any, userBookingDto will be null
        userBookingDto = this.findUserBooking(publicationId, memberId);
        // Get the list of bookings for the publication (need to know nbOfBookings)
        publicationBookings = this.getListBookingByPublicationId( publicationId, BookingStatus.INPROGRESS );

        if (!loanDtos.isEmpty())
            throw new BadRequestException("L'utilisateur a déja emprunté une copie de la publication.");
        else if (userBookingDto != null)
            throw new BadRequestException("L'utilisateur a déja réservé cette publication.");
        else if (publicationBookings.size() >= 2 * publicationDto.getNbTotalOfcopies())
            throw new BadRequestException("La liste des réservations est complète pour cette publication.");
        else if (publicationDto.getNbOfAvailableCopies() != 0)
            throw new BadRequestException("Une copie est disponible pour cette publication.");
        else {

            // Now we can set the different attribute to the booking object
            bookingDto.setBookingDate(today);
            bookingDto.setMember(memberDto);
            bookingDto.setPublication(publicationDto);
            bookingDto.setBookingStatus("INPROGRESS");

            Booking booking = BookingMapper.INSTANCE.toEntity(bookingDto);
            Booking savedBooking = bookingRepository.save(booking);
            savedDtoBooking = BookingMapper.INSTANCE.toDto(savedBooking);

            //Optional.of(booking).ifPresent(booking -> bookingRepository.save(booking));
        }

        return savedDtoBooking;
    }

    @Override
    public BookingDto getBookingById(int bookingId) {
        return BookingMapper.INSTANCE.toDto( bookingRepository.findBookingById(bookingId ).orElse(null));
    }

    @Override
    public BookingDto getBookingByCopyIdAndStatus(int copyId, BookingStatus status) {
        BookingDto bookingDto = BookingMapper.INSTANCE.toDto( bookingRepository.getBookingByCopyIdAndStatus(copyId, status).orElse(null));
        return bookingDto;
    }

    @Override
    public List<BookingDto> getListBookingByPublicationId( int publicationId, BookingStatus status ) {
        List<BookingDto> bookingDtos = BookingMapper.INSTANCE.toDtoList(bookingRepository.findListBookingByPublicationIdAndStatus(publicationId, status));
        return bookingDtos;
    }

    @Override
    public List<BookingDto> getListBookingByUserId(int memeberId) {
        return BookingMapper.INSTANCE.toDtoList(bookingRepository.findListBookingByUserId(memeberId, BookingStatus.INPROGRESS));
    }

    @Override
    public List<BookingDto> getAllBookings() {
        return BookingMapper.INSTANCE.toDtoList(bookingRepository.findAll());
    }

    @Override
    public void updateBookingStatus(int bookingId, BookingStatus status) {

        //Retrieve the booking by its id
        BookingDto bookingDto = BookingMapper.INSTANCE.toDto(bookingRepository.findBookingById(bookingId).orElse(null));

        if (bookingDto == null) {
            throw new ResourceNotFoundException("La réservation recherchée n'existe pas.");

        } else {
            bookingRepository.updateBookingStatus(bookingId, status);
        }
    }

    @Override
    public void updateBookingEmailDate(int bookingId) {
        LocalDate today = LocalDate.now();

        //Retrieve the booking by its id
        BookingDto bookingDto = BookingMapper.INSTANCE.toDto(bookingRepository.findBookingById(bookingId).orElse(null));

        if (bookingDto == null) {
            throw new ResourceNotFoundException("La réservation recherchée n'existe pas.");

        } else {
            bookingRepository.updateBookingEmailDate(bookingId, today);
        }

    }

    @Override
    public Integer countUserPosition(int pubId, int bookingId) {
        return bookingRepository.count(pubId, bookingId, BookingStatus.INPROGRESS);
    }

    /**
     * Retrieve all copies of the publication
     *
     * @param pubId Publication id
     * @return List of the publication copies
     */
    protected List<CopyDto> findAllCopyByPublicationId(int pubId) {
        return CopyMapper.INSTANCE.toDtoList(copyRepository.findAllCopyByPublicationId(pubId));
    }

    /**
     * Try to get all the copies of the publication that the user has borrowed, if there is'nt any, loanDtos will be empty
     *
     * @param copies   List of copies of the pblication
     * @param memberId The member id
     * @return List of loans of the publication copies registered to the user
     */
    protected List<LoanDto> findUserLoan(List<CopyDto> copies, int memberId) {
        List<LoanDto> loanDtos = new ArrayList<>();
        for (CopyDto copy : copies) {
            LoanDto loanDto = LoanMapper.INSTANCE.toDto(loanRepository.findLoanByCopyIdAndUserId(copy.getId(), memberId, LoanStatus.INPROGRESS).orElse(null));
            if (loanDto != null )
                loanDtos.add(loanDto);
        }
        return loanDtos;
    }

    /**
     * Get a booking done by the user for this publication, if there is'nt any, userBookingDto will be null
     *
     * @param pubId    The publication id
     * @param memberId The user id
     * @return A booking registered by the user for a copy of the publication
     */
    protected BookingDto findUserBooking(int pubId, int memberId) {
        Optional<Booking> booking = bookingRepository.findBookingByPubIdAndUserId(pubId, memberId, BookingStatus.INPROGRESS);
        BookingDto bookingDto = BookingMapper.INSTANCE.toDto(booking.orElse(null));
        return bookingDto;
    }

}