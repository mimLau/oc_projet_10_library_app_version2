package com.mimi.mlibrary.service.kafka.producer;

import com.mimi.mlibrary.model.dto.publication.BookingDto;
import com.mimi.mlibrary.model.dto.publication.CopyMemberDto;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class BookingProducer {

    private KafkaTemplate<String, BookingDto> kafkaTemplate;
    private final String TOPIC ="bookings";

    public BookingProducer(KafkaTemplate<String, BookingDto> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void produce( BookingDto bookingDto ) {
        kafkaTemplate.send(TOPIC, bookingDto );
        System.out.println("\n ******************** Booking available, Producer side: bookingID =>: " + bookingDto.getId() + "\n");
    }
}
