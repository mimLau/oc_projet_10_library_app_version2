package com.mimi.mlibrary.service.contract;

import com.mimi.mlibrary.model.dto.account.EmployeeDto;
import com.mimi.mlibrary.model.dto.account.MemberDto;
import com.mimi.mlibrary.model.entity.account.Member;

import java.util.List;

public interface AccountService {

    MemberDto findMemberByUsername( String username );
    List<MemberDto> findAll();
    MemberDto findById( int id );
    MemberDto save( Member member );
    void updateNbOfCurrentsLoans( int id );
    List<MemberDto> getMembersByOutdatedLoan( );
    EmployeeDto findEmployeeByUsername( String username );

}
