package com.mimi.mlibrary.service.impl;

import com.mimi.mlibrary.mapper.account.EmployeeMapper;
import com.mimi.mlibrary.model.dto.account.EmployeeDto;
import com.mimi.mlibrary.model.entity.account.Member;
import com.mimi.mlibrary.model.entity.loan.LoanStatus;
import com.mimi.mlibrary.repository.account.EmployeeRepository;
import com.mimi.mlibrary.repository.account.MemberRepository;
import com.mimi.mlibrary.mapper.account.MemberMapper;
import com.mimi.mlibrary.model.dto.account.MemberDto;
import com.mimi.mlibrary.service.contract.AccountService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class AccountServiceImpl implements AccountService {

    private MemberRepository memberRepository;
    private EmployeeRepository employeeRepository;

    AccountServiceImpl( MemberRepository memberRepository, EmployeeRepository employeeRepository ) {
        this.memberRepository = memberRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public MemberDto findMemberByUsername( String username ) {

        Member member = memberRepository.getMemberByUsername( username ).orElse( null);
        return MemberMapper.INSTANCE.toDto( member );

    }

    @Override
    public EmployeeDto findEmployeeByUsername( String username ) {
        return EmployeeMapper.INSTANCE.toDto( employeeRepository.getEmployeeByUsername( username ).orElse( null) );
    }

    @Override
    public List<MemberDto> findAll() {
        return MemberMapper.INSTANCE.toDtoList( memberRepository.findAll() );
    }

    @Override
    public MemberDto findById( int id ) {
        return MemberMapper.INSTANCE.toDto( memberRepository.findById( id ).orElse(null) );
    }

    @Override
    public MemberDto save( Member member ) {
        //TODO Verify if member already exists

        MemberDto memberDto = MemberMapper.INSTANCE.toDto( memberRepository.save( member ) );
        return memberDto;
    }

    @Override
    public void updateNbOfCurrentsLoans( int id ) {
         memberRepository.updateNbOfCurrentsLoans( id, 1);
    }

    @Override
    public List<MemberDto> getMembersByOutdatedLoan( ) {
        LocalDate currentDate = LocalDate.now();
        return MemberMapper.INSTANCE.toDtoList( memberRepository.getMembersByOutdatedLoan( currentDate , LoanStatus.INPROGRESS ) );
    }




}
