package com.mimi.mlibrary.service.kafka.consumer;

import com.mimi.mlibrary.model.dto.loan.LoanDto;
import com.mimi.mlibrary.service.contract.LoanService;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

@Service
public class LoanReminderConsumer {

    private LoanService loanService;

    public LoanReminderConsumer(LoanService loanService) {
        this.loanService = loanService;
    }

    @KafkaListener(topics = "loanReminder", groupId = "loans_grp_id")
    public void consumer( LoanDto loan ) throws MessagingException {
        System.out.println("\n ***************** Delay loan reminder, Consumer side:  LoanId =>: " + loan.getId() + "\n" );
        loanService.updateReminderNbById( loan.getId() );



    }
}
