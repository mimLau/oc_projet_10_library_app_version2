package com.mimi.mlibrary.service.impl;

import com.mimi.mlibrary.mapper.account.MemberMapper;
import com.mimi.mlibrary.mapper.loan.LoanMapper;
import com.mimi.mlibrary.mapper.publication.BookingMapper;
import com.mimi.mlibrary.mapper.publication.CopyMapper;
import com.mimi.mlibrary.model.dto.account.MemberDto;
import com.mimi.mlibrary.model.dto.loan.LoanDto;
import com.mimi.mlibrary.model.dto.publication.BookingDto;
import com.mimi.mlibrary.model.dto.publication.CopyDto;
import com.mimi.mlibrary.model.entity.loan.Loan;
import com.mimi.mlibrary.model.entity.loan.LoanStatus;
import com.mimi.mlibrary.model.entity.publication.Booking;
import com.mimi.mlibrary.model.entity.publication.BookingStatus;
import com.mimi.mlibrary.model.entity.publication.Copy;
import com.mimi.mlibrary.repository.account.MemberRepository;
import com.mimi.mlibrary.repository.loan.LoanRepository;
import com.mimi.mlibrary.repository.publication.BookingRepository;
import com.mimi.mlibrary.repository.publication.CopyRepository;
import com.mimi.mlibrary.repository.publication.PublicationRepository;
import com.mimi.mlibrary.service.contract.LoanService;
import com.mimi.mlibrary.service.exceptions.BadRequestException;
import com.mimi.mlibrary.service.exceptions.ResourceNotFoundException;
import com.mimi.mlibrary.service.kafka.producer.BookingProducer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class LoanServiceImpl implements LoanService {

    private LoanRepository loanRepository;
    private CopyRepository copyRepository;
    private MemberRepository memberRepository;
    private PublicationRepository publicationRepository;
    private BookingRepository bookingRepository;
    private BookingProducer bookingProducer;


    final static Logger LOGGER  = LogManager.getLogger(LoanServiceImpl.class);

    public LoanServiceImpl(LoanRepository loanRepository, CopyRepository copyRepository, MemberRepository memberRepository, PublicationRepository publicationRepository, BookingRepository bookingRepository, BookingProducer bookingProducer) {

        this.publicationRepository = publicationRepository;
        this.loanRepository = loanRepository;
        this.copyRepository = copyRepository;
        this.memberRepository = memberRepository;
        this.bookingRepository = bookingRepository;
        this.bookingProducer = bookingProducer;
    }


    @Override
    public LoanDto findLoanById( int id ) {
        return   LoanMapper.INSTANCE.toDto( loanRepository.findLoanById( id ).orElse(null));
    }

    @Override
    public List<LoanDto> findAll() {
        return LoanMapper.INSTANCE.toDtoList( loanRepository.findAllLoans( LoanStatus.INPROGRESS));
    }

    @Override
    public List<LoanDto> findByMemberId( int memberId ) {
        List<Loan> loan = loanRepository.findByMemberId( memberId, LoanStatus.INPROGRESS);
        return LoanMapper.INSTANCE.toDtoList( loan );
    }

    /**
     *

     * @param memberId The id of the user who borrows a publication
     * @param copyId The id of the available copy of the publication which will be borrowed
     * @return The created Loan
     */
    @Override
    public LoanDto save( int memberId, int copyId ) {

        int currentsLoans;
        boolean available;
        boolean booked;
        int publicationId;

        // Copy research
        CopyDto copyDto = CopyMapper.INSTANCE.toDto( copyRepository.findCopyById( copyId ).orElse(null));
        // Member research
        MemberDto memberDto = MemberMapper.INSTANCE.toDto( memberRepository.findById( memberId ).orElse(null));


        if( copyDto == null && memberDto == null )
            throw new ResourceNotFoundException("L'utilisateur et la copie demandés n'existent pas.");
        else if( copyDto == null )
            throw new ResourceNotFoundException("La copie demandée n'existe pas.");
        else if( memberDto == null )
            throw new ResourceNotFoundException("L'utilisateur demandé n'existe pas.");
        else {

            currentsLoans = memberDto.getNbOfCurrentsLoans();
            available = copyDto.isAvailable();
            booked = copyDto.isBooked();
            // Retrieve publication id from copy to update publication nbOfAvailableCopies attribute.
            publicationId = copyDto.getPublication().getId();
        }

        /*
         * Member has right to borrow only 5 publications
         * To register or create a Loan of a copy, this one should be available.
         * But There is one case where we can loan a copy even if available is at false,
         * it's when the copy is booked (it means an email was sent to the user,
         * and awaiting to be loaned), and the user in question loans th booked copy.
         */
        if( (currentsLoans < 5 && available) || (currentsLoans < 5 && booked && !available) )   {

            // Retrieve if it exists, the booking of the copy which will be loaned.
            Optional<Booking> booking = bookingRepository.getBookingByCopyIdAndStatus(copyId, BookingStatus.INPROGRESS);

            /* If there is a booking, we update the status of the booking at FINISHED and increment
               the nbOfAvailableCopies of the publication in question (because if a copy is booked, it stay
               unavailable and the nbOfAvailableCopies of the publication isn't updated until the loan is done.
               If we don't increment nbOfAvailableCopies of the publication, further, when we decrement it,
               the value will be distorted.

            */

            if( booking.isPresent() ) {
                bookingRepository.updateBookingStatus(booking.get().getId(), BookingStatus.FINISHED);
                publicationRepository.updateNbOfAvailableCopies( publicationId, 1 );
            }

            LocalDate today = LocalDate.now();

            copyRepository.updateCopyAvailability( false, copyId );
            copyRepository.updateCopyBooking( false, copyId );
            copyRepository.updateCopyReturnDateById( today.plusDays( 28 ),copyId );
            memberRepository.updateNbOfCurrentsLoans( memberId, 1);
            publicationRepository.updateNbOfAvailableCopies( publicationId, -1 );


            //Set attributes to the created loan.

            LoanDto loanDto = new LoanDto();

            loanDto.setCopy( copyDto);
            loanDto.setMember( memberDto);
            loanDto.setReturnDate( today.plusDays( 28 ) );
            loanDto.setLoanDate( today );
            loanDto.setExtented( false );
            loanDto.setLoanStatus( "INPROGRESS" );
            loanDto.setReminderNb( 0 );

            Loan loan = LoanMapper.INSTANCE.toEntity( loanDto );
            Loan savedLoan = loanRepository.save( loan );
            loanDto = LoanMapper.INSTANCE.toDto(savedLoan);

           // Optional.of( LoanMapper.INSTANCE.toEntity( loanDto ) ).ifPresent( loan -> loanRepository.save( loan ));

            return loanDto;

        } else if( currentsLoans == 5 && available == false) {
            throw new BadRequestException("User has already 5 loans and copie isn't available.");

        } else if( currentsLoans == 5 && available == true) {
            throw new BadRequestException("User has already 5 loans.");
        }
        else if( currentsLoans < 5 && available == false ) {
             throw new BadRequestException("This copy isn't available.");
        }

        return null;
    }

    @Override
    public void extendLoanReturnDateById( int loanId ) {

        LocalDate returnDate;
        LocalDate today = LocalDate.now();
        boolean extension;

        //Retrieve a Loan by its id
        LoanDto loanDto = LoanMapper.INSTANCE.toDto(loanRepository.findLoanById(loanId).orElse(null));
        if (loanDto == null) {
            throw new ResourceNotFoundException("Le prêt recherché n'existe pas.");

        } else {

            //Retrieve the return date of this Loan
            returnDate = loanDto.getReturnDate();

            // Verify if the returnDate is outdated, if yes can't do the prolongation
            if (returnDate.isAfter(today) || returnDate.isEqual(today)) {
                LOGGER.error("returnDate: " + returnDate);
                extension = loanDto.isExtented();

                if (extension == false) {

                    //Extend the return date by 4 weeks
                    returnDate = returnDate.plusDays(28);
                    loanRepository.updateLoanReturnDateById(returnDate, loanId);
                    loanRepository.updateExtensionById(loanId, true);

                    int copyId = loanDto.getCopy().getId();
                    // As copy entity has also an returnDate, so we update it too.
                    copyRepository.updateCopyReturnDateById(returnDate, copyId);

                } else {
                    throw new BadRequestException("L'utilisateur a déjà effectué une prolongation pour ce prêt.");
                }

            } else {
                throw new BadRequestException("Vous ne pouvez pas effectuer une prolongation alors que vous avez un retard pour cet ouvrage.");
            }

        }
    }


    @Override
    @Transactional
    public void updateLoanStatus( int loanId ) {

        int copyId;
        int memberId;
        int publicationId;
        List<BookingDto> bookingByPubIdDtos;
        BookingDto bookingDto;
        Integer bookingId;
        Copy copy;

        //Retrieve a Loan by its id
        LoanDto loanDto = LoanMapper.INSTANCE.toDto( loanRepository.findLoanById( loanId ).orElse(null));

        if( loanDto  == null ) {
            throw new ResourceNotFoundException("Le prêt recherché n'existe pas.");

        } else {
             copyId = loanDto.getCopy().getId();
             memberId = loanDto.getMember().getId();

             //We retrieve the publication of the returned copy to search if there is a booking for this publication
             publicationId = loanDto.getCopy().getPublication().getId();

            /* Once a copy is returned, the loan's status change
                But the copy isn't directly available, we have to verify
                if there is a booking for the publication of this copy, if yes, copy stay unavailable, if not we update the attribute available at true.
                Same things for attribute nbOfAvailableCopies of Publication.
             */

           bookingByPubIdDtos = BookingMapper.INSTANCE.toDtoList( bookingRepository.findListBookingByPublicationIdAndStatus(publicationId, BookingStatus.INPROGRESS));

           if( bookingByPubIdDtos.isEmpty() ) {
               copyRepository.updateCopyAvailability(true, copyId);
               publicationRepository.updateNbOfAvailableCopies( publicationId, 1 );
               copyRepository.updateCopyReturnDateById( null, copyId );

           } else if( !bookingByPubIdDtos.isEmpty() ) {

               /* If list of bookings by the publicationId isn't empty, it means there is a booking for this publication,
                  so we retrieve the returned copy to send it by kafka to the mail service module in order to send to the first user in the list of booking an
                  email informing him that a copy of the publication is available.
                */

               copy = copyRepository.findCopyById( copyId ).orElse(null);
               copyRepository.updateCopyBooking(true, copyId); // When an email is send to inform tha ta copy is available, update copy booked value at true.
               bookingId = bookingByPubIdDtos.get(0).getId(); // Retrieve the first booking for the publication
               bookingRepository.updateBookingEmailDate( bookingId, LocalDate.now().plusDays(2)); // and add an email sent date.
               bookingRepository.updateBookingCopy( bookingId, copy ); // add the copy which is set aside for this booking.
               bookingDto =  BookingMapper.INSTANCE.toDto( bookingRepository.findBookingById( bookingId ).orElse(null));
               bookingProducer.produce( bookingDto ); // sent a kafka message with the updated booking to mail application in order to send  an email to the user who did the booking.
           }

            memberRepository.updateNbOfCurrentsLoans( memberId, -1 ); // loan return so decrement nbOfCurrentsLoans
            loanRepository.updateLoanStatus( loanId, LoanStatus.FINISHED ); // loan return so put status of the loan at FINISHED

        }
    }


    @Override
    public void updateReminderNbById( int loanId ) {
        loanRepository.updateReminderNbById( loanId );
    }

    @Override
    public List<LoanDto> findByDelay() {
        LocalDate currentDate = LocalDate.now();
        return  LoanMapper.INSTANCE.toDtoList( loanRepository.findByDelay( currentDate , LoanStatus.INPROGRESS ) );

    }
}
