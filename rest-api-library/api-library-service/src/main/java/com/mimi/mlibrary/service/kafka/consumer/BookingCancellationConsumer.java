package com.mimi.mlibrary.service.kafka.consumer;

import com.mimi.mlibrary.mapper.publication.BookingMapper;
import com.mimi.mlibrary.mapper.publication.CopyMapper;
import com.mimi.mlibrary.model.dto.publication.BookingDto;
import com.mimi.mlibrary.model.dto.publication.PublicationDto;
import com.mimi.mlibrary.model.entity.publication.BookingStatus;
import com.mimi.mlibrary.model.entity.publication.Publication;
import com.mimi.mlibrary.repository.publication.BookingRepository;
import com.mimi.mlibrary.service.contract.BookingService;
import com.mimi.mlibrary.service.kafka.producer.BookingProducer;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class BookingCancellationConsumer {

    private BookingService bookingService;
    private BookingRepository bookingRepository;
    private BookingProducer bookingProducer;
    private BookingDto bookingDto;
    private List<BookingDto> bookingByPubIdDtos;
    private Integer bookingId;

    public BookingCancellationConsumer(BookingService bookingService, BookingRepository bookingRepository, BookingProducer bookingProducer) {
        this.bookingService = bookingService;
        this.bookingRepository = bookingRepository;
        this.bookingProducer = bookingProducer;
    }

    @KafkaListener(topics = "cancellation", groupId = "cancellation_grp")
    @Transactional
    public void consumer( BookingDto booking ) throws MessagingException {
        System.out.println("\n ******************** Booking cancellation Consumer side: BookingId => " + booking.getId() + "\n");
        bookingService.updateBookingStatus( booking.getId(), BookingStatus.CANCELLED );

        bookingByPubIdDtos = bookingService.getListBookingByPublicationId( booking.getPublication().getId(), BookingStatus.INPROGRESS );

       if( !bookingByPubIdDtos.isEmpty() ) {
           bookingId = bookingByPubIdDtos.get(0).getId(); // After cancellation of a booking, retrieve the following booking for the publication

           bookingRepository.updateBookingCopy(bookingId, CopyMapper.INSTANCE.toEntity(booking.getCopy())); // add the copy which is set aside for this booking.
           bookingRepository.updateBookingEmailDate(bookingId, LocalDate.now().plusDays(2));  // add the date  of send of the email
           bookingDto = BookingMapper.INSTANCE.toDto(bookingRepository.findBookingById(bookingId).orElse(null));
           bookingProducer.produce(bookingDto); // sent a kafka message with the updated booking to mail application in order to send  an email to the user who did the booking.
       }
    }
}
