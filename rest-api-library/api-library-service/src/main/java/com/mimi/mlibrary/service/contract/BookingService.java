package com.mimi.mlibrary.service.contract;

import com.mimi.mlibrary.model.dto.publication.BookingDto;
import com.mimi.mlibrary.model.entity.publication.Booking;
import com.mimi.mlibrary.model.entity.publication.BookingStatus;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface BookingService {

    BookingDto save( int publicationId, int memberId );
    BookingDto getBookingById( int bookingId );
    BookingDto getBookingByCopyIdAndStatus( int copyId, BookingStatus status );
    List<BookingDto> getListBookingByPublicationId( int publicationId, BookingStatus status );
    List<BookingDto> getListBookingByUserId( int memberId );
    List<BookingDto>getAllBookings();
    void  updateBookingStatus( int bookingId , BookingStatus status );
    void  updateBookingEmailDate( int booKingId );
    Integer countUserPosition( int pubId, int bookingId );
}
