package com.mimi.mlibrary.service.impl;

import com.mimi.mlibrary.model.dto.publication.BookingDto;
import com.mimi.mlibrary.model.entity.account.Member;
import com.mimi.mlibrary.model.entity.loan.Loan;
import com.mimi.mlibrary.model.entity.loan.LoanStatus;
import com.mimi.mlibrary.model.entity.publication.Booking;
import com.mimi.mlibrary.model.entity.publication.BookingStatus;
import com.mimi.mlibrary.model.entity.publication.Copy;
import com.mimi.mlibrary.model.entity.publication.Publication;
import com.mimi.mlibrary.repository.account.MemberRepository;
import com.mimi.mlibrary.repository.loan.LoanRepository;
import com.mimi.mlibrary.repository.publication.BookingRepository;
import com.mimi.mlibrary.repository.publication.CopyRepository;
import com.mimi.mlibrary.repository.publication.PublicationRepository;
import com.mimi.mlibrary.service.exceptions.BadRequestException;
import com.mimi.mlibrary.service.exceptions.ResourceNotFoundException;
import com.mimi.mlibrary.service.impl.BookingServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BookingServiceImplTest {

    /*@Mock private LoanRepository loanRepositoryMock;
    @Mock private CopyRepository copyRepositoryMock;
    @Mock private MemberRepository memberRepositoryMock;
    @Mock private BookingRepository bookingRepositoryMock;
    @Mock private PublicationRepository publicationRepositoryMock;*/
    private LoanRepository loanRepositoryMock;
    private CopyRepository copyRepositoryMock;
    private MemberRepository memberRepositoryMock;
    private PublicationRepository publicationRepositoryMock;

    //@Spy private BookingRepository bookingRepositorySpy;
    private BookingRepository bookingRepositorySpy;

    //@InjectMocks
    private BookingServiceImpl bookingServiceUnderTest;

    @BeforeEach
    void setUp() {
        loanRepositoryMock = mock(LoanRepository.class);
        copyRepositoryMock = mock(CopyRepository.class);
        memberRepositoryMock = mock(MemberRepository.class);
        publicationRepositoryMock = mock(PublicationRepository.class);

        bookingRepositorySpy = spy(BookingRepository.class);

        bookingServiceUnderTest = new BookingServiceImpl( publicationRepositoryMock, memberRepositoryMock, bookingRepositorySpy, copyRepositoryMock, loanRepositoryMock);
      //  MockitoAnnotations.initMocks(this);
    }

    @Test
    void givenNonExistentPublication_should_ThrowResourceNotFoundException_when_save() {

        //GIVEN
        Member member = new Member();
        member.setId(1);

        Publication publication = new Publication();
        publication.setId(3);

        when(publicationRepositoryMock.findPublicationById(anyInt())).thenReturn(Optional.ofNullable(null));
        given(memberRepositoryMock.findById(1)).willReturn(Optional.of(member));

        // WHEN
        ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> bookingServiceUnderTest.save( publication.getId() , member.getId() ));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("La publication recherchée n'existe pas.");
    }

    @Test
    void givenNonExistentMember_should_ThrowResourceNotFoundException_when_save() {

        //GIVEN
        Member member = new Member();
        member.setId(1);

        Publication publication = new Publication();
        publication.setId(2);

        when(publicationRepositoryMock.findPublicationById(2)).thenReturn(Optional.of(publication));
        given(memberRepositoryMock.findById(anyInt())).willReturn(Optional.ofNullable(null));

        // WHEN
        ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> bookingServiceUnderTest.save( publication.getId(), member.getId()));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("L'utilisateur recherché n'existe pas.");
    }

    @Test
    void givenNonExistentMember_And_NonExistentPublication_should_ThrowResourceNotFoundException_when_save() {

        //GIVEN
        Member member = new Member();
        member.setId(1);

        Publication publication = new Publication();
        publication.setId(2);

        when(publicationRepositoryMock.findPublicationById(anyInt())).thenReturn(Optional.ofNullable(null));
        given(memberRepositoryMock.findById(anyInt())).willReturn(Optional.ofNullable(null));

        // WHEN
        ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> bookingServiceUnderTest.save( publication.getId(), member.getId()));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("L'utilisateur et la publication recherchés n'existent pas.");
    }

    @Test
    void givenLoanNotEmpty_should_ThrowBadRequestException_when_save() {

        //GIVEN
        Member member = new Member();
        member.setId(1);

        Publication publication = new Publication();
        publication.setId(2);

        Copy copy1 = new Copy();
        copy1.setId(1);
        Copy copy2 = new Copy();
        copy2.setId(2);
        Copy copy3 = new Copy();
        copy3.setId(3);

        Loan loan = new Loan();
        loan.setId(1);
        loan.setMember(member);
        loan.setCopy(copy1);

        Booking booking1 = new Booking();
        booking1.setId(1);

        List<Copy> copies = new ArrayList<>();
        copies.add(copy1);
        copies.add(copy2);
        copies.add(copy3);

        List<Booking> bookings = new ArrayList<>();

        when(publicationRepositoryMock.findPublicationById(2)).thenReturn(Optional.of(publication));
        given(memberRepositoryMock.findById(1)).willReturn(Optional.of(member));

        given(copyRepositoryMock.findAllCopyByPublicationId(2) ).willReturn( copies );
        given(loanRepositoryMock.findLoanByCopyIdAndUserId( 1, 1, LoanStatus.INPROGRESS ) ).willReturn(Optional.of(loan));
        doReturn(Optional.ofNullable(null)).when(bookingRepositorySpy).findBookingByPubIdAndUserId(anyInt(), anyInt(), eq(BookingStatus.INPROGRESS) );
        doReturn(bookings).when(bookingRepositorySpy).findListBookingByPublicationIdAndStatus(anyInt(), eq(BookingStatus.INPROGRESS) );

        // WHEN
        BadRequestException exception = assertThrows(
                BadRequestException.class, () -> bookingServiceUnderTest.save( publication.getId(), member.getId()));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("L'utilisateur a déja emprunté une copie de la publication.");
        verify(bookingRepositorySpy).findBookingByPubIdAndUserId(publication.getId(), member.getId(), BookingStatus.INPROGRESS);
        verify(bookingRepositorySpy).findListBookingByPublicationIdAndStatus(publication.getId(), BookingStatus.INPROGRESS);
    }

    @Test
    void givenLoan_With_UserWhoAlreadyBookedThePublication_should_ThrowBadRequestException_when_save() {

        //GIVEN
        Member member = new Member();
        member.setId(1);

        Publication publication = new Publication();
        publication.setId(2);

        Copy copy1 = new Copy();
        copy1.setId(1);
        Copy copy2 = new Copy();
        copy2.setId(2);
        Copy copy3 = new Copy();
        copy3.setId(3);

        Booking booking1 = new Booking();
        booking1.setId(1);
        booking1.setPublication(publication);

        List<Copy> copies = new ArrayList<>();
        copies.add(copy1);
        copies.add(copy2);
        copies.add(copy3);

        List<Booking> bookings = new ArrayList<>();
        bookings.add(booking1);

        when(publicationRepositoryMock.findPublicationById(2)).thenReturn(Optional.of(publication));
        given(memberRepositoryMock.findById(1)).willReturn(Optional.of(member));

        given(copyRepositoryMock.findAllCopyByPublicationId(2) ).willReturn( copies );
        given(loanRepositoryMock.findLoanByCopyIdAndUserId( 1, 1, LoanStatus.INPROGRESS ) ).willReturn(Optional.ofNullable(null));
        doReturn(Optional.of(booking1)).when(bookingRepositorySpy).findBookingByPubIdAndUserId(anyInt(), anyInt(), eq(BookingStatus.INPROGRESS) );
        doReturn(bookings).when(bookingRepositorySpy).findListBookingByPublicationIdAndStatus(anyInt(), eq(BookingStatus.INPROGRESS) );


        // WHEN
        BadRequestException exception = assertThrows(
                BadRequestException.class, () -> bookingServiceUnderTest.save( publication.getId(), member.getId()));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("L'utilisateur a déja réservé cette publication.");
        verify(bookingRepositorySpy).findBookingByPubIdAndUserId(publication.getId(), member.getId(), BookingStatus.INPROGRESS);
        verify(bookingRepositorySpy).findListBookingByPublicationIdAndStatus(publication.getId(), BookingStatus.INPROGRESS);
    }

    @Test
    void givenLoan_With_PublicationWithFullBookings_should_ThrowBadRequestException_when_save() {

        //GIVEN
        Member member = new Member();
        member.setId(1);

        Publication publication = new Publication();
        publication.setId(2);
        publication.setNbTotalOfcopies(1);

        Copy copy1 = new Copy();
        copy1.setId(1);
        Copy copy2 = new Copy();
        copy2.setId(2);
        Copy copy3 = new Copy();
        copy3.setId(3);

        Booking booking1 = new Booking();
        booking1.setId(1);
        Booking booking2 = new Booking();
        booking2.setId(2);

        List<Copy> copies = new ArrayList<>();
        copies.add(copy1);
        copies.add(copy2);
        copies.add(copy3);

        List<Booking> bookings = new ArrayList<>();
        bookings.add(booking1);
        bookings.add(booking2);

        when(publicationRepositoryMock.findPublicationById(2)).thenReturn(Optional.of(publication));
        given(memberRepositoryMock.findById(1)).willReturn(Optional.of(member));

        given(copyRepositoryMock.findAllCopyByPublicationId(2) ).willReturn( copies );
        given(loanRepositoryMock.findLoanByCopyIdAndUserId( anyInt(), anyInt(), eq(LoanStatus.INPROGRESS) )).willReturn(Optional.ofNullable(null));
        doReturn(Optional.ofNullable(null)).when(bookingRepositorySpy).findBookingByPubIdAndUserId(anyInt(), anyInt(), eq(BookingStatus.INPROGRESS) );
        doReturn(bookings).when(bookingRepositorySpy).findListBookingByPublicationIdAndStatus(anyInt(), eq(BookingStatus.INPROGRESS) );

        // WHEN
        BadRequestException exception = assertThrows(
                BadRequestException.class, () -> bookingServiceUnderTest.save( publication.getId(), member.getId()));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("La liste des réservations est complète pour cette publication.");
        verify(bookingRepositorySpy).findBookingByPubIdAndUserId(publication.getId(), member.getId(), BookingStatus.INPROGRESS);
        verify(bookingRepositorySpy).findListBookingByPublicationIdAndStatus(publication.getId(), BookingStatus.INPROGRESS);
    }

    @Test
    void givenBooking_With_AvailablePublication_should_ThrowBadRequestException_when_save() {

        //GIVEN
        Member member = new Member();
        member.setId(1);

        Publication publication = new Publication();
        publication.setId(2);
        publication.setNbOfAvailableCopies(1);
        publication.setNbTotalOfcopies(3);

        Copy copy1 = new Copy();
        copy1.setId(1);
        Copy copy2 = new Copy();
        copy2.setId(2);
        Copy copy3 = new Copy();
        copy3.setId(3);

        Booking booking1 = new Booking();
        booking1.setId(1);
        Booking booking2 = new Booking();
        booking2.setId(2);

        List<Copy> copies = new ArrayList<>();
        copies.add(copy1);
        copies.add(copy2);
        copies.add(copy3);

        List<Booking> bookings = new ArrayList<>();
        bookings.add(booking1);

        when(publicationRepositoryMock.findPublicationById(2)).thenReturn(Optional.of(publication));
        given(memberRepositoryMock.findById(1)).willReturn(Optional.of(member));

        given(copyRepositoryMock.findAllCopyByPublicationId(2) ).willReturn( copies );
        given(loanRepositoryMock.findLoanByCopyIdAndUserId( 1, 1, LoanStatus.INPROGRESS ) ).willReturn(Optional.ofNullable(null));
        doReturn(Optional.ofNullable(null)).when(bookingRepositorySpy).findBookingByPubIdAndUserId(anyInt(), anyInt(), eq(BookingStatus.INPROGRESS) );
        doReturn(bookings).when(bookingRepositorySpy).findListBookingByPublicationIdAndStatus(anyInt(), eq(BookingStatus.INPROGRESS) );

        // WHEN
        BadRequestException exception = assertThrows(
                BadRequestException.class, () -> bookingServiceUnderTest.save( publication.getId(), member.getId()));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("Une copie est disponible pour cette publication.");
        verify(bookingRepositorySpy).findBookingByPubIdAndUserId(publication.getId(), member.getId(), BookingStatus.INPROGRESS);
        verify(bookingRepositorySpy).findListBookingByPublicationIdAndStatus(publication.getId(), BookingStatus.INPROGRESS);
    }


    @Test
    void givenBooking_should_saveBooking_when_save() {

        //GIVEN
        Member member = new Member();
        member.setId(1);

        Publication publication = new Publication();
        publication.setId(2);
        publication.setNbOfAvailableCopies(0);
        publication.setNbTotalOfcopies(1);

        Copy copy1 = new Copy();
        copy1.setId(1);
        Copy copy2 = new Copy();
        copy2.setId(2);
        Copy copy3 = new Copy();
        copy3.setId(3);

        Booking booking1 = new Booking();
        booking1.setId(1);
        Booking booking2 = new Booking();
        booking2.setId(2);

        List<Copy> copies = new ArrayList<>();
        copies.add(copy1);
        copies.add(copy2);
        copies.add(copy3);

        List<Booking> bookings = new ArrayList<>();
        bookings.add(booking1);

        when(publicationRepositoryMock.findPublicationById(2)).thenReturn(Optional.of(publication));
        given(memberRepositoryMock.findById(1)).willReturn(Optional.of(member));

        given(copyRepositoryMock.findAllCopyByPublicationId(2) ).willReturn( copies );
        given(loanRepositoryMock.findLoanByCopyIdAndUserId( 1, 1, LoanStatus.INPROGRESS ) ).willReturn(Optional.ofNullable(null));
        doReturn(Optional.ofNullable(null)).when(bookingRepositorySpy).findBookingByPubIdAndUserId(anyInt(), anyInt(), eq(BookingStatus.INPROGRESS) );
        doReturn(bookings).when(bookingRepositorySpy).findListBookingByPublicationIdAndStatus(anyInt(), eq(BookingStatus.INPROGRESS) );

        // WHEN
        bookingServiceUnderTest.save( publication.getId(), member.getId());

        // THEN
        verify(bookingRepositorySpy).findBookingByPubIdAndUserId(publication.getId(), member.getId(), BookingStatus.INPROGRESS);
        verify(bookingRepositorySpy).findListBookingByPublicationIdAndStatus(publication.getId(), BookingStatus.INPROGRESS);
        // Should throw any exception
    }

    @Test
    void givenPublication_should_returnBookings_when_getListBookingByPublicationId() {

        // GIVEN

        Member member = new Member();
        member.setId(2);

        Publication publication1 = new Publication();
        publication1.setId(1);

        Booking booking1 = new Booking();
        booking1.setPublication(publication1);
        booking1.setMember(member);
        Booking booking2 = new Booking();
        booking2.setPublication(publication1);
        Booking booking3 = new Booking();
        booking3.setPublication(publication1);

        List<Booking> bookings = new ArrayList<>();
        bookings.add(booking1);
        bookings.add(booking2);
        bookings.add(booking3);

        doReturn(bookings).when(bookingRepositorySpy).findListBookingByPublicationIdAndStatus(anyInt(), eq(BookingStatus.INPROGRESS));

        // WHEN
        List <BookingDto> bookingDtos = bookingServiceUnderTest.getListBookingByPublicationId(publication1.getId(),BookingStatus.INPROGRESS );

        // THEN
        verify(bookingRepositorySpy).findListBookingByPublicationIdAndStatus(publication1.getId(), BookingStatus.INPROGRESS);
        assertThat(bookingDtos.size()).isEqualTo(3);

    }

    @Test
    void givenUser_with_should_returnBookings_when_getListBookingByUserId() {

        // GIVEN

        Member member = new Member();
        member.setId(3);

        Publication publication1 = new Publication();
        publication1.setId(1);

        Booking booking1 = new Booking();
        booking1.setPublication(publication1);
        booking1.setMember(member);

        Booking booking2 = new Booking();
        booking2.setPublication(publication1);
        booking2.setMember(member);

        Booking booking3 = new Booking();
        booking3.setPublication(publication1);

        List<Booking> bookings = new ArrayList<>();
        bookings.add(booking1);
        bookings.add(booking2);

        doReturn(bookings).when(bookingRepositorySpy).findListBookingByUserId(anyInt(), eq(BookingStatus.INPROGRESS));

        // WHEN
        List <BookingDto> bookingDtos = bookingServiceUnderTest.getListBookingByUserId(member.getId() );

        // THEN
        verify(bookingRepositorySpy).findListBookingByUserId(member.getId(), BookingStatus.INPROGRESS);
        assertThat(bookingDtos.size()).isEqualTo(2);
    }

    @Test
    void givenListOfBookings_should_returnBookingList_when_getAllBookings() {

        // GIVEN

        Member member = new Member();
        member.setId(3);

        Publication publication1 = new Publication();
        publication1.setId(1);

        Booking booking1 = new Booking();
        booking1.setPublication(publication1);
        booking1.setMember(member);

        Booking booking2 = new Booking();
        booking2.setPublication(publication1);
        booking2.setMember(member);

        Booking booking3 = new Booking();
        booking3.setPublication(publication1);

        List<Booking> bookings = new ArrayList<>();
        bookings.add(booking1);
        bookings.add(booking2);

        doReturn(bookings).when(bookingRepositorySpy).findAll();

        // WHEN
        List <BookingDto> bookingDtos = bookingServiceUnderTest.getAllBookings();

        // THEN
        verify(bookingRepositorySpy, times(1)).findAll();
        assertThat(bookingDtos.size()).isEqualTo(bookings.size());

    }

    @Test
    void givenBooking_with_Status_should_ThrowResourceNotFoundException_when_updateBookingStatus() {

        //GIVEN
        Booking booking1 = new Booking();
        booking1.setId(4);
        booking1.setBookingStatus(BookingStatus.INPROGRESS);

        doReturn(Optional.ofNullable(null)).when(bookingRepositorySpy).findBookingById(anyInt());

        // WHEN
        ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> bookingServiceUnderTest.updateBookingStatus(booking1.getId(), BookingStatus.FINISHED));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("La réservation recherchée n'existe pas.");

    }

    @Test
    void givenBooking_with_Status_should_updateBookingStatus_when_updateBookingStatus() {

        //GIVEN
        Booking booking1 = new Booking();
        booking1.setId(4);
        booking1.setBookingStatus(BookingStatus.INPROGRESS);

        doReturn(Optional.of(booking1)).when(bookingRepositorySpy).findBookingById(anyInt());

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<BookingStatus> statusCaptor = ArgumentCaptor.forClass(BookingStatus.class);


        // WHEN
        bookingServiceUnderTest.updateBookingStatus(booking1.getId(), BookingStatus.FINISHED);

        // THEN
        verify(bookingRepositorySpy, times(1)).updateBookingStatus(idCaptor.capture(), statusCaptor.capture());
        assertThat(idCaptor.getValue()).isEqualTo(booking1.getId());
        assertThat(statusCaptor.getValue()).isNotEqualTo(booking1.getBookingStatus());

    }

    @Test
    void givenBooking_with_Status_should_ThrowResourceNotFoundException_when_updateBookingEmailDate() {

        //GIVEN
        Booking booking1 = new Booking();
        booking1.setId(4);
        booking1.setBookingStatus(BookingStatus.INPROGRESS);

        doReturn(Optional.ofNullable(null)).when(bookingRepositorySpy).findBookingById(anyInt());

        // WHEN
        ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> bookingServiceUnderTest.updateBookingEmailDate(booking1.getId()));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("La réservation recherchée n'existe pas.");

    }

    @Test
    void givenBooking_with_should_UpdateBookingEmailDate_when_updateBookingEmailDate() {

        //GIVEN
        Booking booking1 = new Booking();
        booking1.setId(4);

        doReturn(Optional.of(booking1)).when(bookingRepositorySpy).findBookingById(anyInt());

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<LocalDate> dateCaptor = ArgumentCaptor.forClass(LocalDate.class);

        // WHEN
        bookingServiceUnderTest.updateBookingEmailDate(booking1.getId());

        // THEN
        verify(bookingRepositorySpy, times(1)).updateBookingEmailDate(idCaptor.capture(), dateCaptor.capture());
        assertThat(idCaptor.getValue()).isEqualTo(booking1.getId());
        assertThat(dateCaptor.getValue()).isEqualTo(LocalDate.now());

    }


    @Test
    void givenBooking_with_should_when_countUserPosition() {

        //GIVEN
        Publication publication = new Publication();
        publication.setId(2);
        Booking booking = new Booking();
        booking.setId(4);
        booking.setPublication(publication);
        booking.setBookingStatus( BookingStatus.INPROGRESS );

        doReturn(3).when(bookingRepositorySpy).count(anyInt(), anyInt(), eq( BookingStatus.INPROGRESS ));

        ArgumentCaptor<Integer> idpCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> idbCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<BookingStatus> statusCaptor = ArgumentCaptor.forClass(BookingStatus.class);

        // WHEN
        Integer position = bookingServiceUnderTest.countUserPosition( publication.getId(), booking.getId() );

        // THEN
        verify(bookingRepositorySpy, times(1)).count( idpCaptor.capture(), idbCaptor.capture(), statusCaptor.capture() );
        assertThat(idpCaptor.getValue()).isEqualTo( publication.getId() );
        assertThat(idbCaptor.getValue()).isEqualTo( booking.getId() );
        assertThat(statusCaptor.getValue()).isEqualTo( booking.getBookingStatus() );
        assertThat( position).isEqualTo(3);

    }
}
