package com.mimi.mlibrary.service.impl;

import com.mimi.mlibrary.model.dto.loan.LoanDto;
import com.mimi.mlibrary.model.entity.account.Member;
import com.mimi.mlibrary.model.entity.loan.Loan;
import com.mimi.mlibrary.model.entity.loan.LoanStatus;
import com.mimi.mlibrary.model.entity.publication.Booking;
import com.mimi.mlibrary.model.entity.publication.BookingStatus;
import com.mimi.mlibrary.model.entity.publication.Copy;
import com.mimi.mlibrary.model.entity.publication.Publication;
import com.mimi.mlibrary.repository.account.MemberRepository;
import com.mimi.mlibrary.repository.loan.LoanRepository;
import com.mimi.mlibrary.repository.publication.BookingRepository;
import com.mimi.mlibrary.repository.publication.CopyRepository;
import com.mimi.mlibrary.repository.publication.PublicationRepository;
import com.mimi.mlibrary.service.exceptions.BadRequestException;
import com.mimi.mlibrary.service.exceptions.ResourceNotFoundException;
import com.mimi.mlibrary.service.impl.LoanServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LoanServiceImplTest {

    @Mock private LoanRepository loanRepositoryMock;
    @Mock private CopyRepository copyRepositoryMock;
    @Mock private MemberRepository memberRepositoryMock;
    @Mock private BookingRepository bookingRepositoryMock;
    @Mock private PublicationRepository publicationRepositoryMock;

    @Spy private CopyRepository copyRepository;
    @Spy private MemberRepository memberRepository;
    @Spy private PublicationRepository publicationRepository;
    @Spy private BookingRepository bookingRepository;
    @Spy private LoanRepository loanRepository;

    @InjectMocks
    private LoanServiceImpl loanServiceUnderTest;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    void givenLoan_should_ReturnLoanWithGoodId_When_findLoanById() {

        //GIVEN
        Loan loan = new Loan();
        loan.setId(1);

        //given(loanRepositoryMock.findLoanById(1)).willReturn(Optional.of(loan));
        doReturn(Optional.of(loan)).when(loanRepository).findLoanById(anyInt());

        //WHEN
        LoanDto loanDto = loanServiceUnderTest.findLoanById(1);

        //THEN
        verify(loanRepository).findLoanById(1);
        assertThat(loanDto.getId()).isEqualTo(loan.getId());
    }

    @Test
    void givenLoanList_should_ReturnNotEmptyList_When_findAll() {

        //GIVEN
        List<Loan> loans = new ArrayList<>();
        Loan loan = new Loan();
        loan.setId(1);
        loans.add(loan);

        //when(loanRepositoryMock.findAllLoans(LoanStatus.INPROGRESS)).thenReturn(loans);
        doReturn(loans).when(loanRepository).findAllLoans(LoanStatus.INPROGRESS);

        //WHEN
        List<LoanDto> loanDtos = loanServiceUnderTest.findAll();

        //THEN
        verify(loanRepository).findAllLoans(LoanStatus.INPROGRESS);
        assertThat(loanDtos).isNotEmpty();
    }

    @Test
    void givenLoanList_shouldReturnNotEmptyList_When_findByMemberId() {

        //GIVEN
        Member member1 = new Member();
        member1.setId(4);

        Member member2 = new Member();
        member1.setId(18);

        List<Loan> loans = new ArrayList<>();

        Loan loan1 = new Loan();
        loan1.setId(1);
        loan1.setMember(member1);

        Loan loan2 = new Loan();
        loan2.setId(2);
        loan2.setMember(member2);

        Loan loan3 = new Loan();
        loan3.setId(3);
        loan3.setMember(member2);

        loans.add(loan1);
        loans.add(loan2);
        loans.add(loan3);

        //when(loanRepositoryMock.findByMemberId(2, LoanStatus.INPROGRESS)).thenReturn(loans);
        doReturn(loans).when(loanRepository).findByMemberId(anyInt(), eq(LoanStatus.INPROGRESS));

        //WHEN
        List<LoanDto> loanDtos = loanServiceUnderTest.findByMemberId(2);

        //THEN
        verify(loanRepository).findByMemberId(2, LoanStatus.INPROGRESS);
        assertThat(loanDtos.size()).isEqualTo(loans.size());

    }

    @Test
    public void givenNonExistentCopy_should_ThrowResourceNotFoundException_when_Save() {

        //GIVEN
        Member member = new Member();
        member.setId(1);

        Copy copy = new Copy();
        copy.setId(5);

        doReturn(Optional.ofNullable(null)).when(copyRepository).findCopyById(anyInt());
        doReturn(Optional.of(member)).when(memberRepository).findById(anyInt());

        // WHEN
        ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> loanServiceUnderTest.save( member.getId() , copy.getId()));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("La copie demandée n'existe pas.");
    }

    @Test
    public void givenNoneExistingMember_should_ThrowResourceNotFoundException_when_Save() {

        //GIVEN
        Member member = new Member();
        member.setId(1);

        Copy copy = new Copy();
        copy.setId(5);

        //when(copyRepositoryMock.findCopyById(5)).thenReturn( Optional.of(copy) );
        doReturn( Optional.of(copy) ).when(copyRepository).findCopyById(anyInt());
        //when(memberRepositoryMock.getMemberById(1)).thenReturn( Optional.ofNullable(null));
        doReturn( Optional.ofNullable(null) ).when(memberRepository).findById(anyInt());

        // WHEN
        ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> loanServiceUnderTest.save( member.getId() , copy.getId()));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("L'utilisateur demandé n'existe pas.");
    }

    @Test
    public void givenNoneExistingMemberAndNoneExistingCopy_should_ThrowResourceNotFoundException_when_Save() {

        //GIVEN
        Member member = new Member();
        member.setId(1);

        Copy copy = new Copy();
        copy.setId(5);

        doReturn( Optional.ofNullable(null) ).when(copyRepository).findCopyById(anyInt());
        doReturn( Optional.ofNullable(null) ).when(memberRepository).findById(anyInt());

        // WHEN
        ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> loanServiceUnderTest.save( member.getId() , copy.getId()));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("L'utilisateur et la copie demandés n'existent pas.");
    }

    @Test
    public void givenMember_WithNbOfLoansEqualTo5_And_AvailableCopyAtTrue_should_ThrowBadRequestExceptionException_when_Save() {

        //GIVEN
        Member member = new Member();
        member.setId(1);
        member.setNbOfCurrentsLoans(5);

        Publication publication = new Publication();
        publication.setId(2);

        Copy copy = new Copy();
        copy.setId(5);
        copy.setAvailable(true);
        copy.setPublication( publication );

        doReturn( Optional.of(copy) ).when(copyRepository).findCopyById(anyInt());
        doReturn( Optional.of(member) ).when(memberRepository).findById(anyInt());

        // WHEN
        BadRequestException exception = assertThrows(
                BadRequestException.class, () -> loanServiceUnderTest.save( member.getId() , copy.getId()));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("User has already 5 loans.");
    }

    @Test
    public void givenMember_WithNbOfLoansEqualTo5_And_AvailableCopyAtFalse_should_ThrowBadRequestExceptionException_when_Save() {

        //GIVEN
        Member member = new Member();
        member.setId(1);
        member.setNbOfCurrentsLoans(5);

        Publication publication = new Publication();
        publication.setId(2);

        Copy copy = new Copy();
        copy.setId(5);
        copy.setAvailable(false);
        copy.setPublication( publication );

        doReturn( Optional.of(copy) ).when(copyRepository).findCopyById(anyInt());
        doReturn( Optional.of(member) ).when(memberRepository).findById(anyInt());


        // WHEN
        BadRequestException exception = assertThrows(
                BadRequestException.class, () -> loanServiceUnderTest.save( member.getId() , copy.getId()));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("User has already 5 loans and copie isn't available.");
    }

    @Test
    public void givenMember_WithNbOfLoansLessThan5_And_AvailableCopyAtFalse_should_ThrowBadRequestExceptionException_when_Save() {

        //GIVEN
        Member member = new Member();
        member.setId(1);
        member.setNbOfCurrentsLoans(3);

        Publication publication = new Publication();
        publication.setId(2);

        Copy copy = new Copy();
        copy.setId(5);
        copy.setAvailable(false);
        copy.setPublication( publication );

        doReturn( Optional.of(copy) ).when(copyRepository).findCopyById(anyInt());
        doReturn( Optional.of(member) ).when(memberRepository).findById(anyInt());

        // WHEN
        BadRequestException exception = assertThrows(
                BadRequestException.class, () -> loanServiceUnderTest.save( member.getId() , copy.getId()));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("This copy isn't available.");
    }

    @Test
    public void givenMember_WithNbOfLoansLessThan5_And_AvailableCopyAtTrue_should_CreateNewLoan_when_Save() {

        //GIVEN
        Member member = new Member();
        member.setId(4);
        member.setNbOfCurrentsLoans(3);

        Publication publication = new Publication();
        publication.setId(2);

        Copy copy = new Copy();
        copy.setId(5);
        copy.setAvailable(true);
        copy.setPublication( publication );

        /*
           Use Spy instead of mock because we needs to know if some methods ( updateCopyAvailability, updateCopyReturnDateById ) of copyRepository
           in the method save are really invoked.
           With spy, we have to use doReturn otherwise the stub doesn't work
         */
        doReturn(Optional.of(copy)).when(copyRepository).findCopyById(anyInt());
        doReturn(Optional.of(member)).when(memberRepository).findById(anyInt());
        doReturn(Optional.ofNullable(null)).when(bookingRepository).getBookingByCopyIdAndStatus(anyInt(), eq(BookingStatus.INPROGRESS));
        //when(bookingRepositoryMock.getBookingByCopyIdAndStatus(5, BookingStatus.INPROGRESS)).thenReturn(Optional.ofNullable(null));

        ArgumentCaptor<Boolean> boolCaptor = ArgumentCaptor.forClass(Boolean.class);
        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> intCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<LocalDate> dateCaptor = ArgumentCaptor.forClass(LocalDate.class);


        // WHEN
        LoanDto savedLoan = loanServiceUnderTest.save( member.getId() , copy.getId() );

        // THEN
        verify(copyRepository).updateCopyAvailability( boolCaptor.capture(), idCaptor.capture() );
        assertThat( boolCaptor.getValue() ).isEqualTo( false );
        assertThat( idCaptor.getValue() ).isEqualTo( copy.getId() );

        verify(copyRepository).updateCopyReturnDateById( dateCaptor.capture(), idCaptor.capture() );
        assertThat( dateCaptor.getValue() ).isEqualTo( LocalDate.now().plusDays(28) );
        assertThat( idCaptor.getValue() ).isEqualTo( copy.getId() );

        verify(copyRepository).updateCopyBooking( boolCaptor.capture(), idCaptor.capture() );
        assertThat( boolCaptor.getValue() ).isEqualTo( false );
        assertThat( idCaptor.getValue() ).isEqualTo( copy.getId() );

        verify(memberRepository).updateNbOfCurrentsLoans( idCaptor.capture(), intCaptor.capture() );
        assertThat( idCaptor.getValue() ).isEqualTo( member.getId() );
        assertThat( intCaptor.getValue() ).isEqualTo( 1 );

        verify(publicationRepository, times(1)).updateNbOfAvailableCopies( idCaptor.capture(), intCaptor.capture() );
        assertThat( idCaptor.getValue() ).isEqualTo( publication.getId() );
        assertThat( intCaptor.getValue() ).isEqualTo( -1 );

        //assertThat( savedLoan.getMember().getId() ).isEqualTo( member.getId() );

    }

    @Test
    public void givenMember_WithNbOfLoansLessThan5_And_AvailableCopyAtFalse_And_BookedAtTrue_should_CreateNewLoan_when_Save() {

        //GIVEN
        Member member = new Member();
        member.setId(4);
        member.setNbOfCurrentsLoans(3);

        Publication publication = new Publication();
        publication.setId(2);

        Copy copy = new Copy();
        copy.setId(5);
        copy.setAvailable(false);
        copy.setBooked(true);
        copy.setPublication( publication );

        Booking booking = new Booking();
        booking.setId(3);

        /*
           Use Spy instead of mock because we needs to know if some methods ( updateCopyAvailability, updateCopyReturnDateById ) of copyRepository
           in the method save are really invoked.
           With spy, we have to use doReturn otherwise the stub doesn't work
         */
        doReturn(Optional.of(copy)).when(copyRepository).findCopyById(anyInt());
        doReturn(Optional.of(member)).when(memberRepository).findById(anyInt());
        doReturn(Optional.of(booking)).when(bookingRepository).getBookingByCopyIdAndStatus(anyInt(), eq(BookingStatus.INPROGRESS));
        //when(bookingRepositoryMock.getBookingByCopyIdAndStatus(5, BookingStatus.INPROGRESS)).thenReturn(Optional.of(booking));

        ArgumentCaptor<Boolean> boolCaptor = ArgumentCaptor.forClass(Boolean.class);
        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> intCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<LocalDate> dateCaptor = ArgumentCaptor.forClass(LocalDate.class);
        ArgumentCaptor<BookingStatus> statusCaptor = ArgumentCaptor.forClass(BookingStatus.class);


        // WHEN
        LoanDto savedLoan = loanServiceUnderTest.save( member.getId() , copy.getId() );

        // THEN
        verify(copyRepository).updateCopyAvailability( boolCaptor.capture(), idCaptor.capture() );
        assertThat( boolCaptor.getValue() ).isEqualTo( false );
        assertThat( idCaptor.getValue() ).isEqualTo( copy.getId() );

        verify(copyRepository).updateCopyReturnDateById( dateCaptor.capture(), idCaptor.capture() );
        assertThat( dateCaptor.getValue() ).isEqualTo( LocalDate.now().plusDays(28) );
        assertThat( idCaptor.getValue() ).isEqualTo( copy.getId() );

        verify(copyRepository).updateCopyBooking( boolCaptor.capture(), idCaptor.capture() );
        assertThat( boolCaptor.getValue() ).isEqualTo( false );
        assertThat( idCaptor.getValue() ).isEqualTo( copy.getId() );

        verify(memberRepository).updateNbOfCurrentsLoans( idCaptor.capture(), intCaptor.capture() );
        assertThat( idCaptor.getValue() ).isEqualTo( member.getId() );
        assertThat( intCaptor.getValue() ).isEqualTo( 1 );

        verify(bookingRepository).updateBookingStatus( idCaptor.capture(), statusCaptor.capture());
        assertThat( idCaptor.getValue() ).isEqualTo( booking.getId() );
        assertThat( statusCaptor.getValue() ).isEqualTo( BookingStatus.FINISHED);

        verify(publicationRepository, times(2)).updateNbOfAvailableCopies( idCaptor.capture(), intCaptor.capture() );


       // assertThat( savedLoan.getMember().getId() ).isEqualTo( member.getId() );

    }


    @Test
    void givenLoan_should_ThrowResourceNotFoundException_when_ExtendLoanReturnDateById() {

        // GIVEN
        Loan loan = new Loan();
        loan.setId(2);

        doReturn(Optional.ofNullable(null)).when(loanRepository).findLoanById(anyInt());

        // WHEN
        ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> loanServiceUnderTest.extendLoanReturnDateById(2));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("Le prêt recherché n'existe pas.");

    }

    @Test
    void givenLoan_with_ReturnDateIsBeforeToTodayDate_should_ThrowBadRequestException_when_ExtendLoanReturnDateById() {

        // GIVEN
        LocalDate date = LocalDate.now().minusDays(2);
        Loan loan = new Loan();
        loan.setId(2);
        loan.setReturnDate(date);

        doReturn(Optional.of(loan)).when(loanRepository).findLoanById(anyInt());

        // WHEN
        BadRequestException exception = assertThrows(
                BadRequestException.class, () -> loanServiceUnderTest.extendLoanReturnDateById(2));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("Vous ne pouvez pas effectuer une prolongation alors que vous avez un retard pour cet ouvrage.");

    }

    @Test
    void givenLoan_with_ReturnDateEqualToTodayDate_And_extendedReturnDateAtTrue_should_ThrowBadRequestException_when_ExtendLoanReturnDateById() {

        // GIVEN
        LocalDate date = LocalDate.now().plusDays(2);
        Loan loan = new Loan();
        loan.setId(2);
        loan.setExtented(true);
        loan.setReturnDate(date);

        doReturn(Optional.of(loan)).when(loanRepository).findLoanById(anyInt());

        // WHEN
        BadRequestException exception = assertThrows(
                BadRequestException.class, () -> loanServiceUnderTest.extendLoanReturnDateById(2));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("L'utilisateur a déjà effectué une prolongation pour ce prêt.");
    }

    @Test
    void givenLoan_should_ExtendReturnDate_whenExtendLoanReturnDateById() {

        // GIVEN
        LocalDate date = LocalDate.now().plusDays(2);
        Copy copy = new Copy();
        copy.setId(1);
        Loan loan = new Loan();
        loan.setId(2);
        loan.setExtented(false);
        loan.setReturnDate(date);
        loan.setCopy(copy);

        //when(loanRepositoryMock.findLoanById(2)).thenReturn(Optional.of(loan));
        doReturn(Optional.of(loan)).when(loanRepository).findLoanById(anyInt());

        // WHEN
        loanServiceUnderTest.extendLoanReturnDateById(2);

        // THEN
        // Should not throw exception

    }

    @Test
    void givenLoan_should_ThrowResourceNotFoundException_when_updateLoanStatus() {

        // GIVEN
        Loan loan = new Loan();
        loan.setId(2);

        doReturn(Optional.ofNullable(null)).when(loanRepository).findLoanById(anyInt());

        // WHEN
        ResourceNotFoundException exception = assertThrows(
                ResourceNotFoundException.class, () -> loanServiceUnderTest.extendLoanReturnDateById(2));

        // THEN
        assertThat(exception.getMessage()).isEqualTo("Le prêt recherché n'existe pas.");

    }
    @Test
    void givenLoan_should_UpdateStatusOfLoan_when_updateLoanStatus() {

        // GIVEN
        Publication publication = new Publication();
        publication.setId(5);
        Copy copy = new Copy();
        copy.setId(1);
        copy.setPublication(publication);
        Member member = new Member();
        member.setId(1);
        Loan loan = new Loan();
        loan.setId(2);
        loan.setCopy(copy);
        loan.setMember(member);

        doReturn(Optional.of(loan)).when(loanRepository).findLoanById(anyInt());

        // WHEN
        loanServiceUnderTest.updateLoanStatus(2);

        // THEN
        // Should not throw exception//Comment verifier si le status a bien été changé vu que mes fonctions update oont vomme retour void
        // J'ai vu dans un tuto qu'il rajouté un peti code avec 1 si update est fait et -1 si l'update pas fait dans le service.

    }

    @Test
    void givenLoan_should_incrementLoanReminderNb_when_updateReminderNbById() {

        // Given
        Loan loan = new Loan();
        loan.setId(3);
        loan.setReminderNb(2);

        ArgumentCaptor<Integer> idCpator = ArgumentCaptor.forClass(Integer.class);

        // WHEN
        loanServiceUnderTest.updateReminderNbById( loan.getId());

        // THEN
        verify(loanRepository, times(1)).updateReminderNbById(idCpator.capture());
        assertThat(idCpator.getValue()).isEqualTo(loan.getId());

    }

    @Test
    void givenListOfLoans_should_returnListOfGoodLoans_when_findByDelay() {

        // GIVEN
        List<Loan> loans = new ArrayList<>();
        List<Loan> delayedLoans = new ArrayList<>();

        Loan loan1 = new Loan();
        loan1.setId(1);
        loan1.setReturnDate(LocalDate.now().plusDays(2));

        Loan loan2 = new Loan();
        loan2.setId(2);
        loan2.setReturnDate(LocalDate.now().minusDays(2));

        Loan loan3 = new Loan();
        loan3.setId(3);
        loan3.setReturnDate(LocalDate.now().minusDays(1));

        Loan loan4 = new Loan();
        loan4.setId(4);
        loan4.setReturnDate(LocalDate.now().minusDays(2));

        loans.add(loan1);
        loans.add(loan2);
        loans.add(loan3);
        loans.add(loan4);

        delayedLoans.add(loan2);
        delayedLoans.add(loan3);
        delayedLoans.add(loan4);

        //when(loanRepositoryMock.findByDelay( LocalDate.now() , LoanStatus.INPROGRESS )).thenReturn(delayedLoans);
        doReturn(delayedLoans).when(loanRepository).findByDelay( LocalDate.now() , LoanStatus.INPROGRESS );

        ArgumentCaptor<LocalDate> dateCaptor = ArgumentCaptor.forClass(LocalDate.class);
        ArgumentCaptor<LoanStatus> statusCaptor = ArgumentCaptor.forClass(LoanStatus.class);


        // WHEN
        loanServiceUnderTest.findByDelay();

        // THEN
        verify(loanRepository, times(1)).findByDelay(dateCaptor.capture(), statusCaptor.capture());
        assertThat(dateCaptor.getValue()).isEqualTo( LocalDate.now());
        assertThat(statusCaptor.getValue()).isEqualTo(LoanStatus.INPROGRESS);

    }

}
