package com.mimi.mlibrary.service.impl;

import com.mimi.mlibrary.model.dto.publication.AuthorDto;
import com.mimi.mlibrary.model.dto.publication.CopyDto;
import com.mimi.mlibrary.model.dto.publication.LibraryDto;
import com.mimi.mlibrary.model.dto.publication.PublicationDto;
import com.mimi.mlibrary.model.entity.publication.Author;
import com.mimi.mlibrary.model.entity.publication.Copy;
import com.mimi.mlibrary.model.entity.publication.Library;
import com.mimi.mlibrary.model.entity.publication.Publication;
import com.mimi.mlibrary.repository.publication.AuthorRepository;
import com.mimi.mlibrary.repository.publication.CopyRepository;
import com.mimi.mlibrary.repository.publication.LibraryRepository;
import com.mimi.mlibrary.repository.publication.PublicationRepository;
import com.mimi.mlibrary.service.impl.PublicationServiceImpl;
import com.mimi.mlibrary.specifications.PublicationSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PublicationServiceImplTest {


    @Mock private LibraryRepository libraryRepositoryMock;
    @Mock private PublicationRepository publicationRepositoryMock;
    @Spy private AuthorRepository authorRepositorySpy;
    @Mock private CopyRepository copyRepositoryMock;

    @InjectMocks
    private PublicationServiceImpl publicationServiceUnderTest;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void givenLibrariesList_should_returnSameLibrariesList_when_findAllLibraries() {

        // GIVEN
        Library library1 = new Library();
        library1.setName("Librairie des rossignols");
        Library library2 = new Library();
        List<Library> libraries = new ArrayList<>();

        libraries.add(library1);
        libraries.add(library2);

        when(libraryRepositoryMock.findAllLibraries()).thenReturn(libraries);

        // WHEN
        List<LibraryDto> libraryDtos = publicationServiceUnderTest.findAllLibraries();

        // THEN
        assertThat(libraryDtos.get(0).getName()).isEqualTo(library1.getName());


    }

    @Test
    void givenListPublication_should_returnGoodPublication_when_findPublicationById() {

        // GIVEN
        Author author = new Author();
        author.setFirstname("Guillaume");
        author.setLastname("Musso");
        Publication publication1 = new Publication();
        publication1.setId(1);
        publication1.setAuthor(author);

        when(publicationRepositoryMock.findPublicationById(anyInt())).thenReturn(Optional.of(publication1));

        // WHEN
        PublicationDto publicationDto = publicationServiceUnderTest.findPublicationById( publication1.getId() );

        // THEN
        assertThat(publicationDto.getAuthor().getLastname()).isEqualTo(publication1.getAuthor().getLastname());

    }

    @Test
    void givenListPublication_should_returnAllPublications_when_findAllPublications() {

        // GIVEN
        Author author = new Author();
        author.setFirstname("Guillaume");
        author.setLastname("Musso");
        Publication publication1 = new Publication();
        publication1.setId(5);
        publication1.setAuthor(author);
        Publication publication2 = new Publication();
        publication2.setId(7);
        Publication publication3 = new Publication();

        List<Publication> publications = new ArrayList<>();
        publications.add(publication1);
        publications.add(publication2);
        publications.add(publication3);

        when(publicationRepositoryMock.findAllPublications()).thenReturn(publications);

        // WHEN
        List<PublicationDto> publicationDtos = publicationServiceUnderTest.findAllPublications( );

        // THEN
        assertThat(publicationDtos.get(1).getId()).isEqualTo(publication2.getId());

    }

    /*@Test
    void givenListPublication_should_returnFilteredPublications_when_findAllByCriteria() {

        // GIVEN
        Author author1 = new Author();
        author1.setFirstname("Guillaume");
        author1.setLastname("Musso");

        Author author2 = new Author();
        author2.setFirstname("Jane");
        author2.setLastname("Austen");

        Editor editor1 = new Editor();
        editor1.setName("Pocket");
        Editor editor2 = new Editor();
        editor2.setName("Xo edition");


        Publication publication1 = new Publication();
        publication1.setId(5);
        publication1.setAuthor(author1);
        publication1.setEditor(editor1);
        publication1.setTitle("7 ans après");

        Publication publication2 = new Publication();
        publication2.setId(7);
        publication2.setAuthor(author1);
        publication2.setEditor(editor2);
        publication2.setTitle("7 ans après");

        Publication publication3 = new Publication();
        publication3.setId(4);
        publication3.setAuthor(author2);
        publication3.setTitle("Persuasion ");

        Publication publication4 = new Publication();
        publication4.setId(8);
        publication4.setAuthor(author2);
        publication4.setTitle("Orgueil et Préjugés");

        List<Publication> publications = new ArrayList<>();
        publications.add(publication1);
        publications.add(publication2);


        when(publicationRepositoryMock.findAll()).thenReturn(publications);

        // WHEN
        List<PublicationDto> publicationDtos = publicationServiceUnderTest.findAllByCriteria("Guillaume Musso", "", "", "", 0);

        // THEN
        verify(publicationRepositoryMock, times(1)).findAll();
        assertThat(publicationDtos.get(0).getEditor().getName()).isEqualTo(publications.get(0).getEditor().getName());


    }*/

    @Test
    void givenAuthorList_should_returnAllAuthors_when_findAllAuthor() {

        // GIVEN
        Author author1 = new Author();
        author1.setId(1);
        Author author2 = new Author();
        author2.setId(2);

        List<Author> authors = new ArrayList<>();
        authors.add(author1);
        authors.add(author2);

        doReturn(authors).when(authorRepositorySpy).findAll();

        // WHEN
        List<AuthorDto> authorDtos = publicationServiceUnderTest.findAllAuthor();

        // THEN
        verify(authorRepositorySpy, times(1)).findAll();
        assertThat(authorDtos.size()).isEqualTo(authors.size());
    }


    @Test
    void  givenAuthor_shouldSaveIt_when_saveAuthor() {

        // GIVEN
        Author author = new Author();
        author.setId(2);

        doReturn(author).when(authorRepositorySpy).save( any(Author.class) );

        // WHEN
        Author savedAuthor = publicationServiceUnderTest.saveAuthor( author );

        // THEN
        verify(authorRepositorySpy, times(1)).save( author );
        assertThat(savedAuthor.getId()).isEqualTo(author.getId());

    }

    @Test
    void  givenAuthor_should_deleteIt_when_deleteAuthorById() {

        // GIVEN
        Author author = new Author();
        author.setId(2);

        // WHEN
        publicationServiceUnderTest.deleteAuthorById( author.getId() );

        // THEN
        verify(authorRepositorySpy, times(1)).deleteAuthorById( author.getId() );

    }

    @Test
    void givenCopiesList_should_returnPublicationCopiesList_when_findAllCopyByPublicationId() {

        // GIVEN
        Publication publication1 = new Publication();
        publication1.setId(1);

        Copy copy1 = new Copy();
        copy1.setId(1);
        copy1.setPublication(publication1);
        Copy copy2 = new Copy();
        copy2.setId(2);
        copy2.setBarcode("azefg4sdf");
        copy2.setPublication(publication1);

        List<Copy> copies = new ArrayList<>();
        copies.add(copy1);
        copies.add(copy2);

        when(copyRepositoryMock.findAllCopyByPublicationId(anyInt())).thenReturn( copies );

        // WHEN
        List<CopyDto> copyDtos = publicationServiceUnderTest.findAllCopyByPublicationId( publication1.getId() );

        // THEN
        verify(copyRepositoryMock, times(1)).findAllCopyByPublicationId(publication1.getId());
        assertThat( copyDtos.size()).isEqualTo(copies.size() );
        assertThat( copyDtos.get(1).getBarcode()).isEqualTo(copies.get(1).getBarcode());
    }

    @Test
    void givenCopyList_should_returnCopy_when_findCopyById() {

        // GIVEN
        Copy copy1 = new Copy();
        copy1.setId(1);
        copy1.setBarcode("aze14ddf");
        Copy copy2 = new Copy();
        copy2.setId(2);


        when(copyRepositoryMock.findCopyById(anyInt())).thenReturn(Optional.of(copy1));

        // WHEN
        CopyDto copyDto = publicationServiceUnderTest.findCopyById( copy1.getId());

        // THEN
        verify(copyRepositoryMock, times(1)).findCopyById(copy1.getId());
        assertThat(copyDto.getBarcode()).isEqualTo(copy1.getBarcode());

    }

    @Test
    void givenCopiesList_should_returnDelayedCopiesList_when_findAvailableCopiesByLibrary() {

        // GIVEN
        Copy copy1 = new Copy();
        copy1.setId(1);
        copy1.setReturnDate(LocalDate.now().minusDays(2));
        Copy copy2 = new Copy();
        copy2.setId(2);
        copy2.setReturnDate(LocalDate.now().minusDays(1));

        List<Copy> copies = new ArrayList<>();
        copies.add(copy1);
        copies.add(copy2);

        when(copyRepositoryMock.findAllByDelay(LocalDate.now())).thenReturn( copies );

        // WHEN
        List<CopyDto> copyDtos = publicationServiceUnderTest.findAllCopyByDelay();

        // THEN
        verify(copyRepositoryMock, times(1)).findAllByDelay(LocalDate.now());
        assertThat( copyDtos.size()).isEqualTo(copies.size() );
        assertThat( copyDtos.get(0).getReturnDate()).isEqualTo(copies.get(0).getReturnDate());
    }

    @Test
    void givenListCopies_should_returnAvaibleCopiesListOfALibrary_when_findAvailableCopiesByLibrary() {

        Library library1 = new Library();
        library1.setId(1);

        // GIVEN
        Copy copy1 = new Copy();
        copy1.setId(1);
        copy1.setAvailable(true);
        copy1.setLibrary(library1);

        Copy copy2 = new Copy();
        copy2.setId(2);
        copy2.setAvailable(false);
        copy2.setLibrary(library1);

        Copy copy3 = new Copy();
        copy3.setId(3);
        copy3.setAvailable(true);
        copy3.setLibrary(library1);

        List<Copy> copies = new ArrayList<>();
        copies.add(copy1);
        copies.add(copy3);

        when(copyRepositoryMock.countAllCopyByPublicationIdAndDistinctLib(anyInt())).thenReturn(copies);

        // WHEN
        List<CopyDto> copyDtos = publicationServiceUnderTest.findAvailableCopiesByLibrary( library1.getId() );

        // THEN
        verify(copyRepositoryMock, times(1)).countAllCopyByPublicationIdAndDistinctLib( library1.getId() );
        assertThat( copyDtos.size()).isEqualTo(copies.size() );
        assertThat( copyDtos.get(0).isAvailable()).isEqualTo(true);

    }
}
