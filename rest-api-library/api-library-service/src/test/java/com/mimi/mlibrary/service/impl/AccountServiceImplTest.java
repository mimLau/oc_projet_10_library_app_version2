package com.mimi.mlibrary.service.impl;

import com.mimi.mlibrary.model.dto.account.MemberDto;
import com.mimi.mlibrary.model.entity.account.Member;
import com.mimi.mlibrary.model.entity.loan.Loan;
import com.mimi.mlibrary.model.entity.loan.LoanStatus;
import com.mimi.mlibrary.repository.account.MemberRepository;
import com.mimi.mlibrary.service.impl.AccountServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AccountServiceImplTest {

    @Spy private MemberRepository memberRepositorySpy;

    @InjectMocks
    private AccountServiceImpl accountServiceUnderTest;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void givenUsername_should_returnUser_when_findMemberByUsername() {

        // GIVEN
        String username = "name";
        Member member = new Member();
        member.setAccountOwnerUsername(username);

        doReturn(Optional.of(member)).when(memberRepositorySpy).getMemberByUsername(username);
        ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);

        // WHEN
        MemberDto memberDto = accountServiceUnderTest.findMemberByUsername(username);

        // THEN
        verify(memberRepositorySpy, times(1)).getMemberByUsername(nameCaptor.capture());
        assertThat(nameCaptor.getValue()).isEqualTo(memberDto.getAccountOwnerUsername());
        assertThat(nameCaptor.getValue()).isEqualTo(member.getAccountOwnerUsername());
    }

    @Test
    void givenMemberList_should_return_memberList_when_findAll() {

        // GIVEN
        Member member1 = new Member();
        member1.setId(1);

        Member member2 = new Member();
        member1.setId(2);

        List<Member> members = new ArrayList<>();
        members.add(member1);
        members.add(member2);

        doReturn(members).when(memberRepositorySpy).findAll();

        // WHEN
        List<MemberDto> memberDtos = accountServiceUnderTest.findAll();

        // THEN
        verify(memberRepositorySpy, times(1)).findAll();
        assertThat(memberDtos.size()).isEqualTo(members.size());
    }

    @Test
    void givenMemberList_should_return_member_when_findById() {

        // GIVEN
        Member member1 = new Member();
        member1.setId(1);

        Member member2 = new Member();
        member1.setId(2);

        List<Member> members = new ArrayList<>();
        members.add(member1);
        members.add(member2);

        doReturn(Optional.of(member1)).when(memberRepositorySpy).findById( anyInt() );
        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);

        // WHEN
        MemberDto memberDto = accountServiceUnderTest.findById( member1.getId() );

        // THEN
        verify(memberRepositorySpy, times(1)).findById( idCaptor.capture() );
        assertThat(memberDto.getId()).isEqualTo(idCaptor.getValue());
        assertThat(member1.getId()).isEqualTo(idCaptor.getValue());

    }

    @Test
    void givenMember_should_saveMember_when_save() {

        // GIVEN
        Member member = new Member();
        member.setId(2);

        doReturn(member).when(memberRepositorySpy).save( any(Member.class) );
        ArgumentCaptor<Member> mbCaptor = ArgumentCaptor.forClass(Member.class);

        // WHEN
        MemberDto addedMember = accountServiceUnderTest.save( member );

        // THEN
        verify(memberRepositorySpy, times(1)).save( mbCaptor.capture() );
        assertThat(addedMember.getId()).isEqualTo(mbCaptor.getValue().getId());
        assertThat(member.getId()).isEqualTo(mbCaptor.getValue().getId());

    }

    @Test
    void givenMemberId_should_updateNbOfCurrentrsLoan_when_updateNbOfCurrentsLoans() {

        // GIVEN
        Member member = new Member();
        member.setId(3);
        member.setNbOfCurrentsLoans(2);

        ArgumentCaptor<Integer> idCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> incrementCaptor = ArgumentCaptor.forClass(Integer.class);

        // WHEN
        accountServiceUnderTest.updateNbOfCurrentsLoans(member.getId());

        // THEN
        verify(memberRepositorySpy, times(1)).updateNbOfCurrentsLoans(idCaptor.capture(), incrementCaptor.capture()  );
        assertThat(member.getId()).isEqualTo(idCaptor.getValue());
        assertThat(incrementCaptor.getValue()).isEqualTo(1);
    }

    @Test
    void givenTodayDate_should_returnMemberList_when_returngetMembersByOutdatedLoan() {

        // GIVEN
        Member member1 = new Member();
        member1.setId(1);
        Member member2 = new Member();
        member1.setId(2);

        Loan loan1 = new Loan();
        loan1.setId(1);
        loan1.setReturnDate(LocalDate.now().minusDays(2));
        loan1.setMember(member1);

        Loan loan2 = new Loan();
        loan2.setId(2);
        loan2.setReturnDate(LocalDate.now().minusDays(4));
        loan2.setMember(member2);

        List<Member> members = new ArrayList<>();
        members.add(member1);
        members.add(member2);

        doReturn(members).when(memberRepositorySpy).getMembersByOutdatedLoan( LocalDate.now(), LoanStatus.INPROGRESS );
        ArgumentCaptor<LocalDate> dateCaptor = ArgumentCaptor.forClass(LocalDate.class);
        ArgumentCaptor<LoanStatus> statusCaptor = ArgumentCaptor.forClass(LoanStatus.class);

        // WHEN
        List<MemberDto> memberDtos = accountServiceUnderTest.getMembersByOutdatedLoan();

        // THEN
        verify(memberRepositorySpy, times(1)).getMembersByOutdatedLoan(dateCaptor.capture(), statusCaptor.capture());
        assertThat(memberDtos.size()).isEqualTo(members.size());
        assertThat(dateCaptor.getValue()).isEqualTo(LocalDate.now());
        assertThat(statusCaptor.getValue()).isEqualTo(LoanStatus.INPROGRESS);

    }

}
