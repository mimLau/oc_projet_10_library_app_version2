package com.mimi.mlibrary.model.dto.publication;


import com.mimi.mlibrary.model.dto.account.MemberDto;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SimpleLoanDto {

    private Integer id;
    private LocalDate returnDate;
    private LocalDate loanDate;
    private int reminderNb;
    private boolean extented;
    private String loanStatus;
    private MemberDto member;
}
