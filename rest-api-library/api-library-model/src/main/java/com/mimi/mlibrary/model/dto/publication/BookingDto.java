package com.mimi.mlibrary.model.dto.publication;

import com.mimi.mlibrary.model.dto.account.MemberDto;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookingDto {

    private Integer id;
    private LocalDate bookingDate;
    private LocalDate mailDate;
    private PublicationDto publication;
    private MemberDto member;
    private CopyDto copy;
    private  String bookingStatus;
}
