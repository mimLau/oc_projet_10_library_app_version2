package com.mimi.mlibrary.model.dto.publication;

import com.mimi.mlibrary.model.dto.account.MemberDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CopyMemberDto {

    private CopyDto copy;
    private MemberDto member;


}
