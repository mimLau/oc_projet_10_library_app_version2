package com.mimi.mlibrary.model.dto.publication;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CopyDto {

    private Integer id;
    private String barcode;
    private boolean available;
    private boolean booked;
    private LocalDate returnDate;
    private LibraryDto library;
    private PublicationDto publication;
    private List<SimpleBookingDto> bookings;
    private List<SimpleLoanDto> loans;

}
