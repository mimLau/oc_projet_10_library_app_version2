package com.mimi.mlibrary.model.dto.publication;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LibraryDto {

    private Integer id;
    private String name;
    private String address;
}
