package com.mimi.mlibrary.model.entity.publication;

public enum BookingStatus {

        INPROGRESS,
        FINISHED,
        CANCELLED;
}
