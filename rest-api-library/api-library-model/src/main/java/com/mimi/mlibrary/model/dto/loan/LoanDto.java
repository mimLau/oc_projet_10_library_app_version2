package com.mimi.mlibrary.model.dto.loan;

import com.mimi.mlibrary.model.dto.account.MemberDto;
import com.mimi.mlibrary.model.dto.publication.CopyDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoanDto {

    private Integer id;
    private LocalDate returnDate;
    private LocalDate loanDate;
    private int reminderNb;
    private boolean extented;
    private String loanStatus;
    private CopyDto copy;
    private MemberDto member;

}
