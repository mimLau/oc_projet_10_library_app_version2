package com.mimi.mlibrary.model.entity.publication;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookingJson {

    private Integer member;
    private Integer publication;
}
