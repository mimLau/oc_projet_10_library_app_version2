package com.mimi.mlibrary.model.dto.account;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MemberDto extends AccountDto {

    private String barcode;
    private int nbOfCurrentsLoans;
}
