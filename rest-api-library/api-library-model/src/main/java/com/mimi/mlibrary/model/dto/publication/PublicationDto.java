package com.mimi.mlibrary.model.dto.publication;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import java.time.LocalDate;
import java.util.List;

@Getter @Setter
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PublicationDto {

    private Integer id;
    private int nbOfAvailableCopies;
    private int nbTotalOfcopies;
   // private List<CopyDto> copies;
    private String title;
    private String identificationNb;
    private LocalDate publicationDate;
    private String category;
    private String subCategoryStr;
    private EditorDto editor;
    private AuthorDto author;
    private List<SimpleBookingDto> bookings;



}
