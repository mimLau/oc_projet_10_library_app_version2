package com.mimi.mlibrary.model.dto.publication;

import lombok.*;

import java.util.List;

@Getter @Setter
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class EditorDto {

    private Integer id;
    private String name;
}
