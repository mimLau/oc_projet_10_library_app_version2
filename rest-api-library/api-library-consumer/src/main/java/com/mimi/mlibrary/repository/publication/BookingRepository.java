package com.mimi.mlibrary.repository.publication;

import com.mimi.mlibrary.model.entity.publication.Booking;
import com.mimi.mlibrary.model.entity.publication.BookingStatus;
import com.mimi.mlibrary.model.entity.publication.Copy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Integer>, JpaSpecificationExecutor<Booking> {

    @Query("SELECT b FROM Booking b WHERE b.id= :id")
    Optional<Booking> findBookingById(@Param("id") int id );

    @Query("Select b FROM Booking b JOIN FETCH b.publication p JOIN FETCH b.member m where p.id= :pubId and m.id= :memberId AND b.bookingStatus= :status")
    Optional<Booking> findBookingByPubIdAndUserId(@Param( "pubId" ) int pubId, @Param( "memberId" ) int memberId, @Param("status") BookingStatus status );


    @Query("Select b FROM Booking b JOIN FETCH b.publication p where p.id= :pubId AND b.bookingStatus= :status")
    List<Booking> findListBookingByPublicationIdAndStatus( @Param( "pubId" ) int pubId, @Param("status") BookingStatus status );

    @Query("Select b FROM Booking b JOIN FETCH b.member m where m.id= :memberId and b.bookingStatus= :status")
    List<Booking> findListBookingByUserId( @Param( "memberId" ) int memberId, @Param("status") BookingStatus status );

    @Transactional
    @Modifying
    @Query("Update Booking b SET b.bookingStatus= :status WHERE b.id= :id ")
    void  updateBookingStatus( @Param("id") int id, @Param("status") BookingStatus status );

    @Transactional
    @Modifying(clearAutomatically=true, flushAutomatically = true)
    @Query("Update Booking b SET b.mailDate= :date WHERE b.id= :id ")
    void  updateBookingEmailDate(@Param("id") int id, @Param("date") LocalDate date);

    @Transactional
    @Modifying(clearAutomatically=true, flushAutomatically = true)
    @Query("Update Booking b SET b.copy= :copy WHERE b.id= :id ")
    void  updateBookingCopy(@Param("id") int id, @Param("copy") Copy copy);

    // Count position of a user in the booking list, count all bookings above the user booking in the table
    @Query("Select count(b) FROM Booking b JOIN  b.publication p where p.id= :pubId and b.id < :bookingId and b.bookingStatus= :status")
    Integer count( @Param( "pubId" ) int pubId, @Param( "bookingId" ) int bookingId, @Param("status") BookingStatus status );

    @Query("Select b FROM Booking b JOIN FETCH b.copy c where b.bookingStatus= :status AND c.id= :copyId")
    Optional<Booking> getBookingByCopyIdAndStatus( @Param( "copyId" ) int copyId, @Param("status") BookingStatus status);

}
