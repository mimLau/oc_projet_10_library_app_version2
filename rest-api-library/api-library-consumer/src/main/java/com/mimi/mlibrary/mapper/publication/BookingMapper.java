package com.mimi.mlibrary.mapper.publication;

import com.mimi.mlibrary.model.dto.publication.BookingDto;
import com.mimi.mlibrary.model.dto.publication.SimpleBookingDto;
import com.mimi.mlibrary.model.entity.publication.Booking;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface BookingMapper {

    BookingMapper INSTANCE = Mappers.getMapper(BookingMapper.class);

    BookingDto toDto(Booking booking);
    SimpleBookingDto toSimpleDto(Booking booking);

    List<BookingDto> toDtoList(List<Booking> bookings);

    Booking toEntity(BookingDto bookingDto);



}