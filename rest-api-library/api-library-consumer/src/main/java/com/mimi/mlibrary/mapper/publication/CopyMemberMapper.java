package com.mimi.mlibrary.mapper.publication;

import com.mimi.mlibrary.model.dto.account.MemberDto;
import com.mimi.mlibrary.model.dto.publication.CopyDto;
import com.mimi.mlibrary.model.entity.account.Member;
import com.mimi.mlibrary.model.entity.publication.Copy;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


@Mapper
public interface CopyMemberMapper {

    CopyMemberMapper INSTANCE = Mappers.getMapper( CopyMemberMapper.class );

    @InheritConfiguration( name = "mapMember")
    MemberDto memberToMemberDto( Member member );
    CopyDto copyToCopyDto( Copy copy );

    Member MemberDtoToMemberEntity( MemberDto memberDto );
    Copy CopyDtoToCopyEntity( CopyDto copyDto );
}
