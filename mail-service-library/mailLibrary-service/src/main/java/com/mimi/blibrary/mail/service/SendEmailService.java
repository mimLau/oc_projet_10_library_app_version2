package com.mimi.blibrary.mail.service;



import com.mimi.blibrary.contract.BookingService;
import com.mimi.blibrary.contract.LoanService;
import com.mimi.blibrary.dto.loan.LoanDto;
import com.mimi.blibrary.dto.publication.BookingDto;
import com.mimi.blibrary.dto.publication.CopyDto;
import com.mimi.blibrary.entity.publication.BookingStatus;
import com.mimi.blibrary.kafka.producer.BookingCancellationProducer;
import com.mimi.blibrary.kafka.producer.LoanReminderProducer;
import com.mimi.blibrary.mail.config.EmailService;
import com.mimi.blibrary.mail.config.Mail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@EnableScheduling
@EnableAsync
public class SendEmailService {

    private final Logger LOGGER = LogManager.getLogger( SendEmailService.class );

    private EmailService emailService;
    private LoanService loanService;
    private BookingService bookingService;
    private LoanReminderProducer loanReminderProducer;
    private BookingCancellationProducer bookingCancellationProducer;

    private int dateComparison;


    public SendEmailService(EmailService emailService, LoanService loanService, BookingService bookingService, LoanReminderProducer loanReminderProducer, BookingCancellationProducer bookingCancellationProducer) {
        this.emailService = emailService;
        this.loanService = loanService;
        this.bookingService = bookingService;
        this.loanReminderProducer = loanReminderProducer;
        this.bookingCancellationProducer = bookingCancellationProducer;
    }


    public void sendDelayLoanMail() throws MessagingException {

        List<LoanDto> loans = loanService.findByDelay();


        Map<String, List<LoanDto>> emailLoan = new HashMap<>();

        for (LoanDto l : loans) {
            addValue(l.getMember().getAccountOwnerEmail(), l, emailLoan);
        }

        for (Map.Entry<String, List<LoanDto>> entry : emailLoan.entrySet()) {

            Mail mail = new Mail();
            mail.setFrom("Projet7OCLib@gmail.com");
            mail.setTo(entry.getKey());

           // LOGGER.error("******************* UserEmail: " + entry.getKey());
            mail.setSubject("RETARD DE PRÊT");

            Map<String, Object> model = new HashMap<>();
            model.put("loans", entry.getValue());

            mail.setModel(model);
            emailService.sendSimpleMessage(mail, "reminderEmailTemplate");

            // Each time an email is sending for a recall, send a message to webservice for Incrementing reminder number of a loan
           for (LoanDto loan : entry.getValue())
               loanReminderProducer.produce( loan );

        }
    }

    /*public void sendBookingInfoMail() {
       List<BookingDto>  bookings = bookingService.getAllBookings();

        if( !bookings.isEmpty() ) {
            for (BookingDto booking : bookings) {

                if( !booking.getBookingStatus().equals( BookingStatus.CANCELLED ) && !booking.getBookingStatus().equals( BookingStatus.FINISHED ) && booking.getMailDate() != null ) {

                    dateComparison = LocalDateTime.now().compareTo(booking.getMailDate());
                    if (dateComparison < 0) {

                        bookingCancellationProducer.produce( booking );
                    }
                }
            }
        }
    }*/

    public void sendBookingInfoMail(String destEmail, String firstname, String lastname, CopyDto copy ) throws MessagingException{
        Map<String, Object> model = new HashMap<>();

        Mail mail = new Mail();
        mail.setFrom("Projet7OCLib@gmail.com");
        mail.setTo( destEmail );
        mail.setSubject("Ouvrage disponible");

        model.put("name", firstname + " " + lastname);
        // model.put("userNb", entry.getKey().getBarcode());
        model.put("copy", copy);

        mail.setModel(model);
        emailService.sendSimpleMessage(mail, "availableBookingTemplate");
    }


    private void addValue(String key, LoanDto value, Map<String, List<LoanDto>> map) {
        List<LoanDto> list = map.get(key);
        if (list == null) {
            list = new ArrayList<>();
        }
        list.add(value);
        map.put(key, list);
    }
}
