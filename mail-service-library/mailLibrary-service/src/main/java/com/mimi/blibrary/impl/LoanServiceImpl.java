package com.mimi.blibrary.impl;

import com.mimi.blibrary.contract.LoanService;
import com.mimi.blibrary.dto.loan.LoanDto;
import com.mimi.blibrary.entity.loan.Loan;
import com.mimi.blibrary.entity.loan.LoanStatus;
import com.mimi.blibrary.mapper.loan.LoanMapper;
import com.mimi.blibrary.repository.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class LoanServiceImpl implements LoanService {

    private LoanRepository loanRepository;

    public LoanServiceImpl(LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }


    @Override
    public List<LoanDto> findByDelay() {
        return LoanMapper.INSTANCE.toDtoList( loanRepository.findByDelay(LocalDate.now(), LoanStatus.INPROGRESS));
    }

    @Override
    public void updateReminderNbById( int id ) {
        loanRepository.updateReminderNbById( id );
    }
}
