package com.mimi.blibrary.kafka.consumer;


import com.mimi.blibrary.dto.publication.BookingDto;
import com.mimi.blibrary.mail.service.SendEmailService;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;

@Service
public class BookingConsumer {

    private SendEmailService sendEmailService;

    public BookingConsumer(SendEmailService sendEmailService) {
        this.sendEmailService = sendEmailService;
    }

    @KafkaListener(topics = "bookings", groupId = "bookings_grp_id")
    public void consumer( BookingDto bookingDto ) throws MessagingException {
        System.out.println("\n ******************** Booking available, Consumer side: BookingId =>: " + bookingDto.getId() + "\n");

        sendEmailService.sendBookingInfoMail(bookingDto.getMember().getAccountOwnerEmail(),
                                             bookingDto.getMember().getAccountOwnerFirstname(),
                                             bookingDto.getMember().getAccountOwnerLastname(),
                                             bookingDto.getCopy()
        );


    }
}
