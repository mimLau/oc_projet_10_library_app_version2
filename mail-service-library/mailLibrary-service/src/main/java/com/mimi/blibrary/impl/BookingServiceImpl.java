package com.mimi.blibrary.impl;

import com.mimi.blibrary.dto.publication.BookingDto;
import com.mimi.blibrary.entity.publication.Booking;
import com.mimi.blibrary.mapper.publication.BookingMapper;
import com.mimi.blibrary.repository.BookingRepository;
import com.mimi.blibrary.contract.BookingService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookingServiceImpl implements BookingService {

    private BookingRepository bookingRepository;

    public BookingServiceImpl(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    @Override
    public List<BookingDto> getAllBookings() {
        return BookingMapper.INSTANCE.toDtoList(bookingRepository.findAllBookings());
    }
}
