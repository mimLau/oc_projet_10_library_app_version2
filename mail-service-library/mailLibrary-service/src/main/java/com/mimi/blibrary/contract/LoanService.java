package com.mimi.blibrary.contract;

import com.mimi.blibrary.dto.loan.LoanDto;
import com.mimi.blibrary.entity.loan.Loan;

import java.util.List;

public interface LoanService {

    List<LoanDto> findByDelay();
    void  updateReminderNbById( int id );
}
