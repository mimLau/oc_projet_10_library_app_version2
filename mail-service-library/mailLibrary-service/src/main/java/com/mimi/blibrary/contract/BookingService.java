package com.mimi.blibrary.contract;

import com.mimi.blibrary.dto.publication.BookingDto;
import com.mimi.blibrary.entity.publication.Booking;

import java.util.List;

public interface BookingService {

    List<BookingDto> getAllBookings();
}
