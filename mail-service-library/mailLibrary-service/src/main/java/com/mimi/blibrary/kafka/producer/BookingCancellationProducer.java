package com.mimi.blibrary.kafka.producer;


import com.mimi.blibrary.dto.publication.BookingDto;
import com.mimi.blibrary.entity.publication.Booking;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class BookingCancellationProducer {

    private KafkaTemplate<String, BookingDto> kafkaTemplate;
    private final String TOPIC ="cancellation";

    public BookingCancellationProducer(KafkaTemplate<String, BookingDto> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void produce( BookingDto booking ) {
        kafkaTemplate.send( TOPIC, booking );
        System.out.println("\n ***************** Booking Cancellation Producer side: bookingId=> " + booking.getId() +"\n");
    }
}
