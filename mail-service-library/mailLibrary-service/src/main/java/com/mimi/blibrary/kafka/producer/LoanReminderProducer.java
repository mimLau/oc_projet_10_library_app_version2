package com.mimi.blibrary.kafka.producer;

import com.mimi.blibrary.dto.loan.LoanDto;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class LoanReminderProducer {

    private KafkaTemplate<String, LoanDto> kafkaTemplate;
    private final String TOPIC ="loanReminder";

    public LoanReminderProducer(KafkaTemplate<String, LoanDto> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void produce( LoanDto loan ) {
        kafkaTemplate.send( TOPIC, loan );
        System.out.println("***************** Delay loan reminder, Producer side:  LoanId =>: " + loan.getId() );
    }
}
