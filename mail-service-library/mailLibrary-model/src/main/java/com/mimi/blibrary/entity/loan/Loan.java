package com.mimi.blibrary.entity.loan;

import com.mimi.blibrary.entity.account.Member;
import com.mimi.blibrary.entity.publication.Copy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="loans")
public class Loan implements Serializable {

    @Id
    @GeneratedValue(
            strategy= GenerationType.AUTO,
            generator="native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private Integer id;

    @Column(nullable = false)
    private LocalDate returnDate;

    @Column(nullable = false)
    private LocalDate loanDate;

    @Column(nullable = false)
    @Type(type = "numeric_boolean")
    private boolean extented;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private LoanStatus loanStatus;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "copy_fk")
    private Copy copy;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "member_fk")
    private Member member;

    @Column(nullable = false)
    private int reminderNb;

}
