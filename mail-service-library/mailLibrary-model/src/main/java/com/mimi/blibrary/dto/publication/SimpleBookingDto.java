package com.mimi.blibrary.dto.publication;


import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SimpleBookingDto {

    private Integer id;
    private LocalDate bookingDate;
    private LocalDateTime mailDate;
    private  String bookingStatus;
}
