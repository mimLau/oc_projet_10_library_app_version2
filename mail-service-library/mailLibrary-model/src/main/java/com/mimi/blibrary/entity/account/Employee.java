package com.mimi.blibrary.entity.account;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
@Entity
@DiscriminatorValue("Employee")
public class Employee extends Account {

}
