package com.mimi.blibrary.dto.publication;

import lombok.*;

@Getter
@Setter
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class EditorDto {

    private Integer id;
    private String name;
}
