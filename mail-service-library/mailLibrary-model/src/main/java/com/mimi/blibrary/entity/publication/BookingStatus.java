package com.mimi.blibrary.entity.publication;

public enum BookingStatus {

        INPROGRESS,
        FINISHED,
        CANCELLED;
}
