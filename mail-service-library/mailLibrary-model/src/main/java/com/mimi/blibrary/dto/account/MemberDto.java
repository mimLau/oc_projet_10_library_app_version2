package com.mimi.blibrary.dto.account;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MemberDto extends AccountDto {

    private String barcode;
    private int nbOfCurrentsLoans;

}
