package com.mimi.blibrary.entity.publication;

import com.mimi.blibrary.entity.account.Member;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Table(name="Bookings")
public class Booking {

    @Id
    @GeneratedValue(
            strategy= GenerationType.AUTO,
            generator="native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private Integer id;

    private LocalDate bookingDate;
    private LocalDateTime mailDate;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private BookingStatus bookingStatus;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="publication_fk")
    private Publication publication;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "member_fk")
    private Member member;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "copy_fk")
    private Copy copy;

}
