package com.mimi.blibrary.dto.publication;

import com.mimi.blibrary.dto.account.MemberDto;
import com.mimi.blibrary.entity.account.Member;
import com.mimi.blibrary.entity.publication.Publication;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookingDto {

    private Integer id;
    private LocalDate bookingDate;
    private LocalDateTime mailDate;
    private PublicationDto publication;
    private MemberDto member;
    private CopyDto copy;
    private  String bookingStatus;
}
