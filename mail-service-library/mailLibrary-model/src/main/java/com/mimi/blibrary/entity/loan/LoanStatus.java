package com.mimi.blibrary.entity.loan;

public enum LoanStatus {

    INPROGRESS,
    FINISHED;
}
