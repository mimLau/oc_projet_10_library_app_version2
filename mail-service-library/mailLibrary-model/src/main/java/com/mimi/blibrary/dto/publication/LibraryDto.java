package com.mimi.blibrary.dto.publication;

import lombok.*;

@Getter
@Setter
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class LibraryDto {

    private Integer id;
    private String name;
    private String address;

}
