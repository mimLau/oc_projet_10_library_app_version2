package com.mimi.blibrary.dto.account;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {

    private Integer id;
    private String accountOwnerUsername;
    private String accountOwnerFirstname;
    private String accountOwnerLastname;
    private String accountOwnerEmail;
    private String accountOwnerPhoneNb;
    private boolean activeAccount;
    private String role;
    private LocalDate registrationDate;
}
