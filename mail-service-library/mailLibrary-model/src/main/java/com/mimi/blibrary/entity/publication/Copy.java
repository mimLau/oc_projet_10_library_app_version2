package com.mimi.blibrary.entity.publication;

import com.mimi.blibrary.entity.loan.Loan;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name="Copies")
public class Copy {

    @Id
    @GeneratedValue(
            strategy= GenerationType.AUTO,
            generator="native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private Integer id;

    @Column(nullable = false)
    private String barcode;

    @Column(nullable = false)
    private boolean available;

    private LocalDate returnDate;

    @Column(nullable = false)
    @OneToMany(mappedBy = "copy", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Loan> Loans;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="publication_fk")
    private Publication publication;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "library_fk")
    private Library library;

    @OneToMany(mappedBy = "copy", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Booking> bookings;


}
