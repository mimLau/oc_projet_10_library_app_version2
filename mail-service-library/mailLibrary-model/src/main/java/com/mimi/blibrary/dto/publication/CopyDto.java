package com.mimi.blibrary.dto.publication;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CopyDto {

    private Integer id;
    private String barcode;
    private boolean available;
    private LocalDate returnDate;
    private PublicationDto publication;
    private List<SimpleBookingDto> bookings;

}
