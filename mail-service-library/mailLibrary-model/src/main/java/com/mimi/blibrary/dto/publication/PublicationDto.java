package com.mimi.blibrary.dto.publication;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PublicationDto {

    private Integer id;
    private int nbOfAvailableCopies;
    private int nbTotalOfcopies;
    private String title;
    private String identificationNb;
    private LocalDate publicationDate;
    private String category;
    private String subCategoryStr;
    private EditorDto editor;
    private AuthorDto author;
    private List<SimpleBookingDto> bookings;
}
