package com.mimi.blibrary.entity.account;

public enum Role {

    USER,
    ADMIN,
    LIBRARY;
}
