package com.mimi.blibrary.mapper.account;

import com.mimi.blibrary.dto.account.AccountDto;
import com.mimi.blibrary.entity.account.Account;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface AccountMapper {

    AccountMapper INSTANCE = Mappers.getMapper( AccountMapper.class);
    AccountDto toDto(Account account );
    List<AccountDto> toDtolist(List<Account> accounts );

}
