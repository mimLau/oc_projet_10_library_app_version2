package com.mimi.blibrary.repository;


import com.mimi.blibrary.entity.loan.Loan;
import com.mimi.blibrary.entity.loan.LoanStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Integer>, JpaSpecificationExecutor<Loan> {

    @Query("SELECT l FROM Loan l WHERE l.returnDate < :current_date AND l.loanStatus= :status")
    List<Loan> findByDelay(@Param("current_date") LocalDate currentDate, @Param("status") LoanStatus status );

    @Transactional
    @Modifying
    @Query("Update Loan l SET l.reminderNb= l.reminderNb + 1 WHERE l.id= :id")
    void  updateReminderNbById( @Param("id") int id );
}
