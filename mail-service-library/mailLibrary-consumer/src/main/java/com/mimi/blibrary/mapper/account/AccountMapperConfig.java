package com.mimi.blibrary.mapper.account;

import com.mimi.blibrary.dto.account.AccountDto;
import com.mimi.blibrary.entity.account.Account;
import org.mapstruct.MapperConfig;
import org.mapstruct.MappingTarget;

@MapperConfig
public interface AccountMapperConfig {

    void mapAccount(Account account, @MappingTarget AccountDto accountDto );
}
