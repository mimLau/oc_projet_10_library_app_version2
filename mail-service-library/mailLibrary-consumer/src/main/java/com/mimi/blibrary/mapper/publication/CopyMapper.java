package com.mimi.blibrary.mapper.publication;



import com.mimi.blibrary.dto.publication.CopyDto;
import com.mimi.blibrary.entity.publication.Copy;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface CopyMapper {

    CopyMapper INSTANCE = Mappers.getMapper( CopyMapper.class );

    CopyDto toDto(Copy copy );
    List<CopyDto> toDtoList(List<Copy> copies );

    Copy toEntity(CopyDto copyDto );
    List<Copy> toEntityList( List<CopyDto> copyDtos );
}
