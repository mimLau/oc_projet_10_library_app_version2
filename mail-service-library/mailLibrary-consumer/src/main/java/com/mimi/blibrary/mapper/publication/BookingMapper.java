package com.mimi.blibrary.mapper.publication;


import com.mimi.blibrary.dto.publication.BookingDto;
import com.mimi.blibrary.dto.publication.SimpleBookingDto;
import com.mimi.blibrary.entity.publication.Booking;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface BookingMapper {

    BookingMapper INSTANCE = Mappers.getMapper(BookingMapper.class);

    BookingDto toDto(Booking booking);
    SimpleBookingDto toSimpleDto(Booking booking);

    List<BookingDto> toDtoList(List<Booking> bookings);
    Booking toEntity(BookingDto bookingDto);
}
