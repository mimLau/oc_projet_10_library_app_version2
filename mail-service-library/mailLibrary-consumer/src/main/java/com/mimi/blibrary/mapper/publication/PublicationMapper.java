package com.mimi.blibrary.mapper.publication;

import com.mimi.blibrary.dto.publication.PublicationDto;
import com.mimi.blibrary.entity.publication.Publication;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface PublicationMapper {

    // By convention, a mapper interface should define a member called INSTANCE
    // which holds a single instance of the mapper type:
    PublicationMapper INSTANCE = Mappers.getMapper(PublicationMapper.class);

    @Mapping(target = "subCategoryStr", source = "subCategory", qualifiedBy = SubCategoryMapper.class)
    PublicationDto toDto(Publication publication);
    List<PublicationDto> toDtoList(List<Publication> publications);

    Publication toEntity(PublicationDto publicationDto);
    List<Publication> toListEntity(List<PublicationDto> publicationDtos);
}
