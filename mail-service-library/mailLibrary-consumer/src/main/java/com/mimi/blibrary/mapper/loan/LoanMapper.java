package com.mimi.blibrary.mapper.loan;

import com.mimi.blibrary.dto.loan.LoanDto;
import com.mimi.blibrary.entity.loan.Loan;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface LoanMapper {

    LoanMapper INSTANCE = Mappers.getMapper( LoanMapper.class );
    LoanDto toDto(Loan loan );
    List<LoanDto> toDtoList(List<Loan> loans );

    Loan toEntity(LoanDto loanDto );
    List<Loan> toEntityList( List<LoanDto> loanDtos );
}
