package com.mimi.blibrary.mapper.account;

import com.mimi.blibrary.dto.account.MemberDto;
import com.mimi.blibrary.entity.account.Member;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.MapperConfig;
import org.mapstruct.MappingTarget;

@MapperConfig
public interface MemberMapperConfig extends AccountMapperConfig {

    @InheritConfiguration( name = "mapAccount")
    void mapMember(Member member, @MappingTarget MemberDto memberDto );
}
