package com.mimi.blibrary.mapper.account;

import com.mimi.blibrary.dto.account.MemberDto;
import com.mimi.blibrary.entity.account.Member;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper( config = MemberMapperConfig.class )
public interface MemberMapper {

    MemberMapper INSTANCE = Mappers.getMapper( MemberMapper.class );

    @InheritConfiguration( name = "mapMember")
    MemberDto toDto( Member member );
    List<MemberDto> toDtoList(List<Member> members );

    Member toEntity( MemberDto memberDto );
    List<Member> toEntityList( List<MemberDto> memberDtos );
}
