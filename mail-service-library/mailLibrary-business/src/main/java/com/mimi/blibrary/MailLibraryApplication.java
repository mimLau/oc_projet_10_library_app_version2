package com.mimi.blibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MailLibraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(MailLibraryApplication.class, args);
	}

}
