package com.mimi.blibrary.task;

import com.mimi.blibrary.contract.BookingService;
import com.mimi.blibrary.dto.publication.BookingDto;
import com.mimi.blibrary.dto.publication.CopyDto;
import com.mimi.blibrary.entity.publication.BookingStatus;
import com.mimi.blibrary.entity.publication.Copy;
import com.mimi.blibrary.kafka.producer.BookingCancellationProducer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class BookingVerification {

    final static Logger logger = LogManager.getLogger(BookingVerification.class);
    private BookingCancellationProducer bookingCancellationProducer;
    private BookingService bookingService;
    private int dateComparison;

    public BookingVerification(BookingCancellationProducer bookingCancellationProducer, BookingService bookingService) {
        this.bookingCancellationProducer = bookingCancellationProducer;
        this.bookingService = bookingService;
    }

    @Transactional
    public void sendBookingInfoMail() {

        List<BookingDto> bookings = bookingService.getAllBookings();

        if (!bookings.isEmpty()) {
            for (BookingDto booking : bookings) {

                if (booking.getBookingStatus().equals("INPROGRESS")  && booking.getMailDate() != null) {

                    dateComparison = LocalDateTime.now().compareTo(booking.getMailDate());
                    if (dateComparison > 0) {

                        bookingCancellationProducer.produce(booking);
                        logger.info( "\n\n ****************** Cancellation message sent for booking id " + booking.getId() + ".\n");


                    } else {
                        logger.info( "****************** No delay in reservations for booking id :" + booking.getId() +  "\n");
                    }

                } else if( !booking.getBookingStatus().equals("INPROGRESS")) {
                    logger.info( "****************** Booking id: " + booking.getId() + " was :" + booking.getBookingStatus() +  "\n");

                } else if (booking.getMailDate() == null) {
                    logger.info( "****************** No mail sending yet for booking id " + booking.getId() + " .\n");
                }
            }

        } else {
            logger.info( "\n\n ****************** No current reservation  \n");
        }
    }
}
