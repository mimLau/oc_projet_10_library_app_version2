package com.mimi.blibrary.launcher;

import com.mimi.blibrary.mail.service.SendEmailService;
import com.mimi.blibrary.task.BookingVerification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@EnableScheduling
@EnableAsync
public class ScheduledTask {

    private final Logger LOGGER = LogManager.getLogger( ScheduledTask.class );
    private SendEmailService sendEmailService;
    private BookingVerification bookingVerification;

    public ScheduledTask(SendEmailService sendEmailService, BookingVerification bookingVerification) {
        this.sendEmailService = sendEmailService;
        this.bookingVerification = bookingVerification;
    }


     /*
        Scheduled every day at 01:00 am
        @Scheduled(cron="0 0 1 * * ?")
    */

    //Scheduled every 1 minute
    @Scheduled(cron = "0 */1 * * * ?")
    public void launchTask() throws Exception {
        Date date = new Date();

        /*LOGGER.info( "\n\n *********************** Send delay loan mail: Scheduler starts at " + date + ".\n");
        sendEmailService.sendDelayLoanMail();*/

        LOGGER.info( "\n\n *********************** Send Booking verification availability mail: Scheduler starts at " + date + ".\n");
        bookingVerification.sendBookingInfoMail();


    }
}
